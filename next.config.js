module.exports = {
    webpack: (config, { dev }) => {
        config.module.rules.push({
            test: /\.(css|scss)/,
            loader: 'emit-file-loader',
            options: {
                name: 'dist/[path][name].[ext]'
            }
        }, {
            test: /\.css$/,
            loader: 'babel-loader!raw-loader'
        }, {
            test: /\.scss$/,
            loader: 'babel-loader!raw-loader!sass-loader'
        })
        return config
    },
    publicRuntimeConfig: {
        API_TOKEN_MONGO: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImphbWFycXVleiIsInBhc3N3b3JkIjoiMTI1MDIyMTkifQ.CyCSVhqAo06zbeFb-GWtaN5cnN0pWeHu883IB1N0J0g',
        API_JOBBY_URI: 'https://nameless-garden-21839.herokuapp.com',
        API_FAKE_URI: 'http://localhost:5000'
    }
}