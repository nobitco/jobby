import React from "react";
import { Container, Row, Col, Input, Button, Modal, ModalHeader, ModalBody } from "reactstrap";
import Page from "../components/page";
import Layout from "../components/layout";
import SideNavBar from "../components/sideNavBar";
import { studentMenu } from "../components/menu-models";
import AsyncData from "../components/async-data";
export default class extends Page {
  static async getInitialProps({ req }) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({ req });
    // it do request for dashboard
    try {
        props.assessments = await AsyncData.getAssessments(props.session.user.username);
    } catch(e) {
        props.error = "Unable to fetch on server!";
    }
    return props;
  }
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      context: "resumen", // It's can be practicantes || tutores || entregas || resumen
      selectedUsers: []
    };
  }
  //sets the content according to state.context
  getContent = context => {
    switch (context) {
      case "practicantes":
        return (
          <div>
              <h3 style={{ marginTop: 40 }}>we are developmenting this feature! :)</h3>
          </div>
        );
        break;
      case "entregas":
        return <Assesments items={this.props.assessments} />;
        break;
    }
  };

  handleContextChange = selectedItem => {
    if (selectedItem.id !== this.state.context) {
      if (this.state.selectedUsers.length > 0) {
        if (
          confirm(
            "Tienes usuarios elementos seleccionados. ¿Está seguro de cambiar de contexto(los elementos seleccionados se eliminarán)?"
          )
        ) {
          this.setState({ selectedUsers: [] });
          this.setState({ context: selectedItem.id });
        }
      } else {
        this.setState({ context: selectedItem.id });
      }
    }
  };

  render() {
    if (!this.props.session.user) return super.adminAcccessOnly();

    return (
      <Layout
        {...this.props}
        hasHeader={true}
        hasFooter={false}
        page={"dashboard"}
      >
      <Row>
        {this.props.session.user && (
          <Col xs="1" sm="1" md="3" lg="3" id="sidebar-container">
            <SideNavBar
              onContextChange={this.handleContextChange}
              navItems={studentMenu}
              currentContext={this.state.context}
            />
          </Col>
        )}
        <Col
          xs={{ size: 12, offset: 1 }}
          sm={{ size: 11, offset: 1 }}
          md={{ size: 9, offset: 2 }}
          lg={{ size: 9, offset: 2 }}
          id="content-container"
        >
          {this.props.session.user && this.props.session.user.role !== 1
            ? this.getContent(this.state.context)
            : super.adminAcccessOnly()}
          {!this.props.session.user && (
            <h2 className="text-center display-4 mt-5 mb-2">
              No te encuentras autenticado :/
            </h2>
          )}
        </Col>
      </Row>
      </Layout>
    );
  }
}

function Assesments(props) {
  let { items } = props;
  return (
    <ul className="list-group" id="assesments-list">
      {items !== undefined ? (
        items.map((item, key) => (
          <li className="list-group-item-action list-item" key={key}>
            <span className="float-left">{item.order}</span>
            <div>
              <a className="d-flex" href="">
                <h2>{item.title}</h2>
              </a>
              <small>Se entrega a los {item.startAt} días</small>
              <p>{item.description}</p>
            </div>
          </li>
        ))
      ) : (
        <h3 style={{ marginTop: 40 }}>No Entregas creadas en tu cuenta :/</h3>
      )}
    </ul>
  );
}
