import Page from "../../components/page";
import Router from "next/router";
import Layout from "../../components/layout";
import {
  Container,
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
  Alert,
  InputGroup,
  InputGroupAddon,
  Button,
  Input
} from "reactstrap";
import fetch from "isomorphic-fetch";

export default class extends Page {
  static async getInitialProps({ req }) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({ req });

    const res = await fetch(
      "https://powerful-mountain-15092.herokuapp.com/api/tutors"
    );

    props.tutores = await res.json();
    return props;
  }

  constructor(props) {
    super(props);
    // to indicate navitem active
    let itemSw = [false, false, true, false, false];

    this.state = {
      navItemActive: 2,
      navItemsw: itemSw
    };
    this.handleClick = this.handleClick.bind(this);
    return;
  }

  handleClick(event, name) {
    event.preventDefault();

    console.log(name);
  }

  handleClicknav(e, id, page) {
    e.preventDefault();

    let itemSw = [false, false, false, false];
    itemSw[id] = true;
    itemSw[this.state.navItemActive] = false;

    this.setState({
      navItemActive: id,
      navItemsw: itemSw
    });

    if (page == "dashboard") {
      Router.push("/dashboard-coordinador/");
    } else {
      Router.push("/coordinador/" + page);
    }
  }
  render() {
    const alert = this.state.alert ? (
      <Alert color="success" isOpen={this.state.visibleAlert}>
        {this.state.msgalert}
      </Alert>
    ) : (
      ""
    );
    const styleListItem =
      "list-group-item list-group-item-action flex-column align-items-start";
    const styleActivedListItem =
      "list-group-item list-group-item-action flex-column align-items-start active";

    return (
      <Layout
        {...this.props}
        hasFooter={true}
        navmenu={false}
        container={false}
      >
        <Container style={{ position: "fixed", marginLeft: 0, maxWidth: 200 }}>
          <Row>
            <Col xs="12" sm="12" className="pt-5">
              <div>
                <Nav vertical pills>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 0, "dashboard")}
                      active={this.state.navItemsw[0]}
                    >
                      <i className="fas fa-home" /> Dashboard
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 1, "practicantes")}
                      active={this.state.navItemsw[1]}
                    >
                      <i className="fas fa-book-reader" /> Practicantes
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 2, "tutores")}
                      active={this.state.navItemsw[2]}
                    >
                      <i className="fas fa-graduation-cap" /> Tutores
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 3, "empresas")}
                      active={this.state.navItemsw[3]}
                    >
                      <i className="fas fa-city" /> Empresas
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 4, "jefes")}
                      active={this.state.navItemsw[4]}
                    >
                      <i className="fab fa-black-tie" /> Jefes
                    </NavLink>
                  </NavItem>
                </Nav>
              </div>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row className="pb-0" style={{ height: "70px" }}>
            <Col xs="10" sm={{ size: 10, offset: 2 }} className="pt-2">
              {alert}
            </Col>
          </Row>
          <Row className="pb-1">
            <Col xs="10" sm={{ size: 5, offset: 2 }} className="pt-0">
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <Button>Buscar</Button>
                </InputGroupAddon>
                <Input />
              </InputGroup>
            </Col>
            <Col xs="10" sm={{ size: "auto", offset: 3 }} className="pt-0">
              <Button
                color="primary"
                className="float-right"
                onClick={this.toggleModalUser}
              >
                Nuevo
              </Button>
            </Col>
          </Row>

          <Row className="pb-5 pt-0">
            <Col xs="10" sm={{ size: 10, offset: 2 }} className="pt-5">
              <div className="list-group">
                {this.props.tutores.map((tutor, i) => (
                  <ItemGroup
                    key={i}
                    name={i}
                    styleListItem={styleListItem}
                    onClick={this.handleClick}
                    t={tutor}
                  />
                ))}
              </div>
            </Col>
          </Row>
        </Container>
      </Layout>
    );
  }
}

class ItemGroup extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <a
        href="#"
        className={this.props.styleListItem}
        onClick={e => this.props.onClick(e, this.props.name)}
      >
        <div className="d-flex w-100 justify-content-between">
          <h5 className="mb-1">
            {this.props.t.lastname + " " + this.props.t.name}
          </h5>
          <small>
            <img
              src={this.props.t.avatar}
              className="avatar"
              style={{ width: "50px", height: "50px" }}
            />
          </small>
        </div>
        <p className="mb-1">{this.props.t.email}</p>
        <small>
          {this.props.t.university + " - Colombia"}{" "}
          <span className="badge badge-danger badge-pill">14</span>{" "}
        </small>
      </a>
    );
  }
}
