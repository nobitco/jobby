import React from "react";
import {
  Container,
  Row,
  Col,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Alert
} from "reactstrap";
import Page from "../components/page";
import Layout from "../components/layout";
import SideNavBar from "../components/sideNavBar";
import { adminMenu } from "../components/menu-models";
import AsyncData from "../components/async-data";
import { CreateUpdateUserForm, DeleteForm } from "../components/crud-forms";
export default class extends Page {
  static async getInitialProps({ req }) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({ req });

    let response = await AsyncData.getUsers();
    if (response.success) {
      props.users = response.data
      props.fetchMessage = "¡Usuarios encontrados satisfactoriamente!";
    } else {
      props.users = [];
      props.fetchMessage = response.msg;
    }
    return props;
  }

  constructor(props) {
    super(props);
    this.state = {
      context: "usuarios",
      selectedUsers: [],
      filterText: "",
      showModal: false,
      modalForm: "", // expected to be "create/update-form" || "delete-form"
      crudUser: null,
      modalResponse: "",
      users: props.users,
      msg: props.fetchMessage
    };
  }

  async componentDidMount() {
    /*
    if (this.props.users === null) {
      try {
        this.props.users = await AsyncData.getUsers();
        this.props.fetchMessage = "¡Usuarios encontrados satisfactoriamente!";
        this.setState({ users: this.props.users })
      } catch (e) {
        this.props.fetchMessage = "Unable to fetch AsyncData on server";
      }
    }
    */
    console.log('didMount')
  }

  handleOnSearch = filterText => {
    this.setState({ filterText: filterText });
  };
  handleListItemSelection = clickedItem => {
    const { isChecked, item } = clickedItem;
    let selectedUsers = [...this.state.selectedUsers];
    var index = this.state.selectedUsers.indexOf(item);
    if (index < 0 && isChecked) {
      selectedUsers.push(item);
    } else {
      selectedUsers.splice(index, 1);
    }
    this.setState({ selectedUsers: selectedUsers });
  };

  toggleAllSelection = (isChecked, listItems) => {
    let selectedUsers = [...this.state.selectedUsers];

    isChecked
      ? listItems.forEach(item => {
          selectedUsers.indexOf(item) < 0 && selectedUsers.push(item);
        })
      : (selectedUsers.length = 0);
    this.setState({ selectedUsers: selectedUsers });
    console.log(isChecked + "    " + selectedUsers);
  };

  handleContextChange = selectedItem => {
    console.log(selectedItem.id);
    if (selectedItem.id !== this.state.context) {
      if (this.state.selectedUsers.length > 0) {
        if (
          confirm(
            "Tienes usuarios elementos seleccionados. ¿Está seguro de cambiar de contexto(los elementos seleccionados se eliminarán)?"
          )
        ) {
          this.setState({ selectedUsers: [] });
          this.setState({ context: selectedItem.id });
        }
      } else {
        this.setState({ context: selectedItem.id });
      }
    }
  };

  toggleModal = e =>
    this.setState({
      showModal: !this.state.showModal,
      modalForm: "",
      crudUser: null
    });

  setModalForm = (modalForm, user) => {
    this.setState({
      showModal: !this.state.showModal,
      modalForm: modalForm,
      crudUser: user
    });
  };
  /*if (page == "dashboard") {   EXAMPLE OF PUSHING ROUTING
      Router.push("/dashboard-coordinador/");
    } else {
      Router.push("/coordinador/" + page);
    }*/
  //sets the content according to state.context
  getContent = context => {
    switch (context) {
      case "resumen":
        return <Estatistics />;
        break;
      case "usuarios":
        return (
          <div>
            <Button
              color="primary"
              className="float-right"
              onClick={e => this.setModalForm("create/update-form", null)}
            >
              Nuevo Usuario
            </Button>
            <ListLayout
              items={this.state.users}
              selectedItems={this.state.selectedUsers}
              onItemSelected={this.handleListItemSelection}
              onToggleAllSelection={this.toggleAllSelection}
              onSearch={this.handleOnSearch}
              searchText={this.state.filterText}
              onEditItem={this.setModalForm}
            />
          </div>
        );
        break;
      default:
        return <Estatistics />;
    }
  };

  handleSubmitCreatingUpdatingUser = async (msg, operation) => {
    this.setState({ modalResponse: msg, showModal: false })
    let response = await AsyncData.getUsers();
    if (response.success) {
      this.setState({ users: response.data, msg: msg });
    } else {
      this.setState({ msg: response.msg })
    }
  }

  render() {
    if (!this.props.session.user) return super.adminAcccessOnly();
    
    return (
      <Layout
        {...this.props}
        hasHeader={true}
        hasFooter={false}
        page={"dashboard"}
      >
        <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>
            {!this.state.crudUser
              ? "Crear nuevo usuario"
              : "Actualizar datos de usuario"}
          </ModalHeader>
          <ModalBody>
            {this.state.modalForm === "create/update-form" && (
              <CreateUpdateUserForm
                onSubmit={this.handleSubmitCreatingUpdatingUser}
                onCancel={this.toggleModal}
                user={this.state.crudUser}
              />
            )}

            {this.state.modalForm === "delete-form" && (
              <DeleteForm
                user={this.state.crudUser}
                onSubmit={this.handleSubmitCreatingUpdatingUser}
                onCancel={this.toggleModal}
              />
            )}
          </ModalBody>
        </Modal>
        <Row>
          {this.props.session.user && (
            <Col sm="2" md="3" lg="2" xl="2">
              <SideNavBar
                onContextChange={this.handleContextChange}
                navItems={adminMenu}
                currentContext={this.state.context}
              />
            </Col>
          )}
          <Col
            xs={{ size: 12}}
            sm={{ size: 10}}
            md={{ size: 9}}
            lg={{ size: 10}}
            lg={{ size: 10}}
            id="content-container"
          >
            <Alert color="warning">
              { this.state.msg }
            </Alert>
            {this.props.session.user && this.props.session.user.admin
              ? this.getContent(this.state.context)
              : super.notAuthorized()}
          </Col>
        </Row>
      </Layout>
    );
  }
}

function Estatistics(props) {
  return (
    <Container>
      <Row className="pb-5">
        <Col xs="12" sm={{ size: 10 }}>
          <div className="card-deck">
            <div className="card" style={{ width: "18rem" }}>
              <div className="card-body">
                <h5 className="card-title">Practicantes</h5>
                <p className="card-text">Total de practicantes.</p>
                <a href="#" className="btn btn-primary">
                  ver
                </a>
              </div>
            </div>
            <div className="card" style={{ width: "18rem" }}>
              <div className="card-body">
                <h5 className="card-title">Tutores</h5>
                <p className="card-text">Total de tutores con sistema.</p>
                <a href="#" className="btn btn-primary">
                  ver
                </a>
              </div>
            </div>
            <div className="card" style={{ width: "18rem" }}>
              <div className="card-body">
                <h5 className="card-title">Ofertas</h5>
                <p className="card-text">Total de ofertas.</p>
                <a href="#" className="btn btn-primary">
                  ver
                </a>
              </div>
            </div>
          </div>
        </Col>
      </Row>
      <Row className="pb-5">
        <Col xs="12" sm={{ size: 10 }} className="pt-5">
          <div className="card-deck">
            <div className="card" style={{ width: "18rem" }}>
              <div className="card-body">
                <h5 className="card-title">Empresas</h5>
                <p className="card-text">Total de exmpresas.</p>
                <a href="#" className="btn btn-primary">
                  ver
                </a>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

function ListLayout(props) {
  const filteredItems = props.items.filter(item => filtrar(item));
  //filters per fullname, but it should iterate every item key and check if its value coincides!
  function filtrar(obj) {
    let isName;
    let isLastname;

    obj["name"]
    .toLowerCase()
    .indexOf(props.searchText.toLowerCase()) >= 0
    ? isName = true
    : isName = false;

    obj["lastname"]
    .toLowerCase()
    .indexOf(props.searchText.toLowerCase()) >= 0
    ? isLastname = true
    : isLastname = false;

    return((isName || isLastname) ? true : false);
  }

  //console.log(filteredItems.length > 0 && filteredItems);

  return (
    <div>
      <div className="search-bar-container">
        <i className="fas fa-search" />
        <Input
          type="text"
          onChange={e => props.onSearch(e.target.value)}
          className="search-input"
          placeholder="Buscar"
          value={props.searchText}
        />
      </div>
      <div id="list-info-container">
        <Input
          type="checkbox"
          onChange={e =>
            props.onToggleAllSelection(e.target.checked, props.items)
          }
          checked={props.selectedItems.length > 0 ? true : false}
        />
        <p>{filteredItems.length} Resultados</p>
      </div>
      <ul className="list-group">
        {filteredItems.length > 0 ? (
          filteredItems.map((item, key) => (
            <ListItem
              key={key}
              onSelected={props.onItemSelected}
              isSelected={props.selectedItems.indexOf(item) < 0 ? false : true}
              onEdit={props.onEditItem}
              item={item}
            />
          ))
        ) : (
          <h3 style={{ marginTop: 40 }}>No hay resultados para mostrar :/</h3>
        )}
      </ul>
    </div>
  );
}

function ListItem(props) {
  //lift the selected item up and its checkbox current on/off state to add or remove it
  //in the selectedUsers array state
  function handleChange(e) {
    const clickedItem = {
      isChecked: e.target.checked,
      item: props.item
    };
    props.onSelected(clickedItem);
  }
  const { item } = props;
  let roleCaption;
  switch(item.role) {
    case '2':
      roleCaption = "coordinador"
      break;
    case '3':
      roleCaption = "tutor"
      break;
    case '4':
      roleCaption = "practicante"
      break;
  }
  return (
    <li
      className={
        "list-group-item list-group-item-action list-item" +
        (props.isSelected ? " selected" : "")
      }
    >
      <Input
        type="checkbox"
        onChange={handleChange}
        className="item-checkbox"
        id={item.id}
        checked={props.isSelected}
      />
      {item.avatar && <img src={item.avatar} className="avatar" />}
      <div className="item-info">
        <h3 className="mb-1">{item.name + ' ' + item.lastname}</h3>
        {item.admin && (
          <span className="badge badge-danger badge-pill">SuperAdmin</span>
        )}
        {!item.admin && (
          <span className="badge badge-info badge-pill">{ roleCaption }</span>
        )}
        <p className="mb-1">{item.email}</p>
        <small>{item.username}</small>
      </div>

      <div className="item-opts">
        <button
          href=""
          className="delete-btn"
          onClick={e => props.onEdit("delete-form", item)}
        >
          <i className="material-icons">&#xe872;</i>
        </button>
        <button
          href=""
          className="edit-btn"
          onClick={e => props.onEdit("create/update-form", item)}
        >
          <i className="material-icons">&#xe254;</i>
        </button>
      </div>
    </li>
  );
}
