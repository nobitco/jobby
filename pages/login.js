import Link from 'next/link'
import Head from 'next/head'
import React from 'react'
import { Container, Row, Col, Form, FormGroup, Label, Input, FormText, Button, Alert } from 'reactstrap'
import Page from '../components/page'
import Layout from '../components/layout'
import Router from 'next/router'
import { NextAuth } from 'next-auth/client'
import Styles from '../css/index.scss'
export default class extends Page {
  static async getInitialProps({req, res, query}) {
    let props = await super.getInitialProps({req})
    props.session = await NextAuth.init({force: true, req: req})
    props.providers = await NextAuth.providers({req})
    
    // If signed in already, redirect to account management page.
    if (props.session.user) {
      if (req) {
        if (props.session.user.admin) {
          res.redirect('/dashboard-admin')  
        } else {
          if (props.session.user.role == 1) {
            res.redirect('/dashboard-coordinador')  
          }
          res.redirect('/')
        }
        res.redirect('/')
      } else {
        if (props.session.user.admin) {
          Router.push('/dashboard-admin')  
        } else {
          if (props.session.user.role == 1) {
            Router.push('/dashboard-coordinador')  
          }
          Router.push('/')
        }
        Router.push('/')
      }
    }

    // If passed a redirect parameter, save it as a cookie
    if (query.redirect) {
      const cookies = new Cookies((req && req.headers.cookie) ? req.headers.cookie : null)
      cookies.set('redirect_url', query.redirect, { path: '/' })
    }
    
    return props
  }

  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      session: this.props.session,
      failedAuth: false,
      authenticating: false
    }
    this.handleEmailChange = this.handleEmailChange.bind(this)
    this.handlePasswordChange = this.handlePasswordChange.bind(this)
    this.handleSignInSubmit = this.handleSignInSubmit.bind(this)
  }

  async componentDidMount() {
    if (this.props.session.user) {
      Router.push(`/auth/`)
    }
  }
  
  handleEmailChange(event) {
    this.setState({
      email: event.target.value
    })
  }

  handlePasswordChange(event) {
    this.setState({
      password: event.target.value
    })
  }
  handleSignInSubmit(event) {
    event.preventDefault()
    this.setState({ authenticating: true })
    // An object passed NextAuth.signin will be passed to your signin() function
    NextAuth.signin({
      email: this.state.email,
      password: this.state.password
    })
    .then(authenticated => {
      Router.push(`/auth/callback`)
    })
    .catch(() => {

      this.setState({
        failedAuth: true, authenticating: false, password: ''
      })
      document.getElementById("email").focus()
      document.getElementById("email").select()
    })
  }
  render() {
    let titleLogin

    this.state.authenticating ? titleLogin = '  Autenticando!' : titleLogin = '  Entrar!'
    return (
      <React.Fragment>
        <Head>
          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1"/>
          <title>{this.props.title || 'jobby'}</title>
          <style dangerouslySetInnerHTML={{__html: Styles}}/>
          <script src="https://cdn.polyfill.io/v2/polyfill.min.js"/>
          <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"></link>
        </Head>
        <Container>
          <Row className="align-items-center">
            <Col xs="12" md={{ size: 4, offset: 4 }} className="pt-5">
              <h1 className="text-center text-primary">jobby</h1>
            </Col>
          </Row>          
          <Row style={{height: '70px'}}>
            <Col xs={{ size: 10, offset: 1 }} sm={{ size:10, offset:1 }} md={{ size: 8, offset: 2 }} className="mt-2">
              {this.state.failedAuth &&
                <Alert color="warning">
                  ¡Usuario o password incorrecto!
                </Alert>
              } 
            </Col>
          </Row>
          <Row>
            <Col xs={{ size: 10, offset: 1 }} sm={{ size:10, offset:1 }} md={{ size: 8, offset: 2 }} lg={{ size: 6, offset: 3 }} xl={{ size: 6, offset: 3 }}>
              <Form id="signin" method="post" action="/auth/sigin" onSubmit={ this.handleSignInSubmit }>
                <Input name="_csrf" type="hidden" value={this.state.session.csrfToken}/>
                <FormGroup>
                    <Label for="exampleEmail">Email</Label>
                    <Input name="email" type="email" id="email" className="form-control" placeholder="j.mqz@example.com" value={this.state.email} onChange={this.handleEmailChange}/>
                </FormGroup>
                <FormGroup>
                    <Label for="examplePassword">Password</Label>
                    <Input name="password" type="password" placeholder="" id="password" className="form-control" value={this.state.password} onChange={this.handlePasswordChange}/>
                </FormGroup>
                <FormGroup><a href="#" className="text-secondary font-italic">¿Olvido el password?</a></FormGroup>
                <Button id="submitButton" outline color="dark" type="submit" block>
                  { this.state.authenticating && 
                    <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                  }
                  { titleLogin }
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    )
  }
}
