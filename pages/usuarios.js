import Link from "next/link";
import Router from "next/router";
import React from "react";
import Page from "../components/page";
import Layout from "../components/layout";
import {
  Container,
  Row,
  Col,
  Table,
  Button,
  InputGroup,
  InputGroupAddon,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  Nav,
  NavItem,
  NavLink,
  Navbar,
  Alert
} from "reactstrap";
import CreateUserForm from "../components/createUserForm";
import AsyncData from "../components/async-data";
export default class extends Page {
  static async getInitialProps({ req }) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({ req });

    // If running on server, perform Async call
    // if (typeof window === 'undefined') {
    try {
      props.users = await AsyncData.getUsers();
      props.msgalert = "¡Usuarios encontrados satisfactoriamente!";
      props.alert = true;
    } catch (e) {
      props.error = "Unable to fetch AsyncData on server";
    }
    // }
    return props;
  }

  // Set data on page load (only if prop is populated, i.e. running on server)
  constructor(props) {
    super(props);

    let itemSw = [false, true, false, false, false, false];
    /*
      modeForm if is true then render form for create 
      userto object for update or any thing
      idTimer
    */
    this.state = {
      modal: false,
      modeForm: false,
      users: props.users || null,
      navItemActive: 1,
      navItemsw: itemSw,
      alert: props.alert,
      msgalert: props.msgalert,
      visibleAlert: true,
      userto: {},
      idTimer: null
    };

    this.toggleModalUser = this.toggleModalUser.bind(this);
    this.handleEditUser = this.handleEditUser.bind(this);
    this.handleDeleteUser = this.handleDeleteUser.bind(this);
    this.handleClicknav = this.handleClicknav.bind(this);
    this.timerAlert = this.timerAlert.bind(this);
    this.alertShow = this.alertShow.bind(this);
  }

  // This is called after rendering, only on the client (not the server)
  // This allows us to render the page on the client without delaying rendering,
  // then load the data fetched via an async call in when we have it.
  async componentDidMount() {
    // Only render posts client side if they are not populate (if the page was
    // rendered on the server, the state will be inherited from the server
    // render by the client)
    if (this.state.users === null) {
      try {
        this.setState({
          users: await AsyncData.getUsers(),
          error: null
        });
      } catch (e) {
        this.setState({
          error: "Unable to fetch AsyncData on client"
        });
      }
    }
    let id = setInterval(this.timerAlert, 4000);
    this.setState({
      idTimer: id
    });
  }

  toggleModalUser(e) {
    if (e) {
      e.preventDefault();
    }
    let usernew = {
      username: "",
      password: ""
    };
    // set modeForm to new user
    this.setState({
      modal: !this.state.modal,
      modeForm: true,
      userto: usernew
    });
  }

  handleEditUser(i) {
    // set modeForm to update
    this.setState({
      modal: !this.state.modal,
      modeForm: false,
      userto: this.state.users[i - 1]
    });
  }

  async handleDeleteUser(i) {
    // alert('delete user '+ this.state.users[i-1].username)
    try {
      let data = await AsyncData.deleteUser(this.state.users[i - 1].username);
      this.alertShow("¡Usuario fue eliminado!");
    } catch (e) {
      this.alertShow("¡Eliminar usuario fallo!");
    }
  }

  handleClicknav(e, id, page) {
    e.preventDefault();

    let itemSw = [false, false, false, false, false, false];
    itemSw[id] = true;
    itemSw[this.state.navItemActive] = false;

    this.setState({
      navItemActive: id,
      navItemsw: itemSw
    });

    Router.push("/" + page);
  }

  // off alert
  timerAlert() {
    this.setState({
      visibleAlert: false
    });
    clearInterval(this.state.idTimer);
  }

  // Turn on alert and start timer
  alertShow(msg) {
    let id = setInterval(this.timerAlert, 4000);
    this.setState({
      visibleAlert: true,
      msgalert: msg,
      idTimer: id
    });
  }
  render() {
    const alert = this.state.alert ? (
      <Alert color="success" isOpen={this.state.visibleAlert}>
        {this.state.msgalert}
      </Alert>
    ) : (
      ""
    );
    const styleListItem =
      "list-group-item list-group-item-action flex-column align-items-start";

    return (
      <Layout {...this.props} navmenu={false} container={false}>
        <Container style={{ position: "fixed", marginLeft: 0, maxWidth: 200 }}>
          <Row>
            <Col xs="12" sm="12" className="pt-5">
              <div>
                <Nav vertical pills>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e =>
                        this.handleClicknav(e, 0, "dashboard-admin")
                      }
                      active={this.state.navItemsw[0]}
                    >
                      <i className="fas fa-home" /> Dashboard
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 1, "usuarios")}
                      active={this.state.navItemsw[1]}
                    >
                      <i className="fas fa-user" /> Usuarios
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 2, "perfiles")}
                      active={this.state.navItemsw[2]}
                    >
                      <i className="fas fa-id-card-alt" /> Perfiles
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 3, "parametros")}
                      active={this.state.navItemsw[3]}
                    >
                      <i className="fas fa-file-alt" /> Parámetros
                    </NavLink>
                  </NavItem>
                </Nav>
                <hr />
                <Nav vertical pills>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 4, "universidades")}
                      active={this.state.navItemsw[4]}
                    >
                      <i className="fas fa-university" /> Universidades
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      href="#"
                      onClick={e => this.handleClicknav(e, 5, "practicantes")}
                      active={this.state.navItemsw[5]}
                    >
                      <i className="fas fa-book-reader" /> Practicantes
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink disabled href="#">
                      <i className="fas fa-city" /> Empresas
                    </NavLink>
                  </NavItem>
                </Nav>
              </div>
            </Col>
          </Row>
        </Container>
        <Container>
          <Row className="pb-0" style={{ height: "70px" }}>
            <Col xs="10" sm={{ size: 10, offset: 2 }} className="pt-2">
              {alert}
            </Col>
          </Row>
          <Row className="pb-1">
            <Col xs="10" sm={{ size: 5, offset: 2 }} className="pt-0">
              <InputGroup>
                <InputGroupAddon addonType="prepend">
                  <Button>Buscar</Button>
                </InputGroupAddon>
                <Input />
              </InputGroup>
            </Col>
            <Col xs="10" sm={{ size: "auto", offset: 3 }} className="pt-0">
              <Button
                color="primary"
                className="float-right"
                onClick={this.toggleModalUser}
              >
                Nuevo
              </Button>
            </Col>
          </Row>

          <Row className="pb-5 pt-0">
            <Col xs="10" sm={{ size: 10, offset: 2 }} className="pt-5">
              {/*
              <UsersTable users={ this.state.users } handleEditUser={this.handleEditUser} 
              handleDeleteUser={this.handleDeleteUser} />
              */}
              <div className="list-group">
                {this.state.users.map((user, i) => (
                  <ItemGroup
                    key={i}
                    name={i}
                    styleListItem={styleListItem}
                    onClick={this.handleClick}
                    onClickUpdate={this.handleEditUser}
                    onClickDelete={this.handleDeleteUser}
                    u={user}
                  />
                ))}
              </div>
            </Col>
          </Row>
        </Container>
        <UserForm
          modal={this.state.modal}
          toggleModalUser={this.toggleModalUser}
          modeForm={this.state.modeForm}
          user={this.state.userto}
          alertShow={this.alertShow}
        />
      </Layout>
    );
  }
}

export class UserForm extends React.Component {
  render() {
    let header;
    if (this.props.modeForm) {
      header = "Nuevo usuario";
    } else {
      header = "Datos usuario";
    }
    return (
      <Modal isOpen={this.props.modal} toggle={this.props.toggleModalUser}>
        <ModalHeader toggle={this.props.toggleModalUser}>{header}</ModalHeader>
        <ModalBody>
          <CreateUserForm
            modeForm={this.props.modeForm}
            toggle={this.props.toggleModalUser}
            user={this.props.user}
            alertShow={this.props.alertShow}
          />
        </ModalBody>
      </Modal>
    );
  }
}
export class UsersTable extends React.Component {
  handleEditClick(i, e) {
    e.preventDefault();
    this.props.handleEditUser(i + 1);
    console.log(i);
  }

  handleDeleteClick(i, e) {
    e.preventDefault();
    this.props.handleDeleteUser(i + 1);
  }
  render() {
    return (
      <React.Fragment>
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>username</th>
              <th>email</th>
              <th>opciones</th>
            </tr>
          </thead>
          <tbody>
            {this.props.users.map((user, i) => (
              <tr key={i + 1}>
                <th scope="row">{i + 1}</th>
                <td>{user.username}</td>
                <td>{user.username}</td>
                <td>
                  <a href="" onClick={e => this.handleEditClick(i, e)}>
                    <i className="material-icons">&#xe254;</i>
                  </a>
                  <a href="" onClick={e => this.handleDeleteClick(i, e)}>
                    <i className="material-icons">&#xe872;</i>
                  </a>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </React.Fragment>
    );
  }
}

class ItemGroup extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className={this.props.styleListItem}>
        <a href="#" onClick={e => this.props.onClick(e, this.props.name)}>
          <div className="d-flex w-100 justify-content-between">
            <h5 className="mb-1">{this.props.u.fullname}</h5>
            <small>
              <img
                src="/static/user.png"
                className="avatar"
                style={{ width: "50px", height: "50px" }}
              />
            </small>
          </div>
          <p className="mb-1">{this.props.u.username}</p>
          <small>
            {this.props.u.username + " - " + this.props.u.username}{" "}
            <span className="badge badge-danger badge-pill">14</span>{" "}
          </small>
        </a>
        <div className="d-flex flex-row-reverse bd-highlight">
          <a
            href=""
            onClick={e => this.props.onClickDelete(e, this.props.name)}
          >
            <i className="material-icons">&#xe872;</i>
          </a>
          <a
            href=""
            onClick={e => this.props.onClickUpdate(e, this.props.name)}
          >
            <i className="material-icons">&#xe254;</i>
          </a>
        </div>
      </div>
    );
  }
}
