import Link from "next/link";
import React from "react";
import {
  Container,
  Row,
  Col,
  Button,
  Jumbotron,
  Badge
} from "reactstrap";
import Page from "../components/page";
import Layout from "../components/layout";

export default class extends Page {
  constructor(props) {
    super(props);
    this.state = {
      sloganStudent: 'practicantes: mas profesional y la mejor experiencia!',
      sloganBusiness: 'empresas: visibilidad para practicantes',
      sloganUniversity: 'universidades: eficiente gestión del proceso',
      sloganStudentUpdated: '',
      sloganBusinessUpdated: '',
      sloganUniversityUpdated: '',
      speed: 50,
      indexTyped: 0,
      idSloganCurrent: 0
    }
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.typeWriter(),
      80
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  typeWriter() {
    let i
    let idCurrent
    let sloganCurrent
    let stringUpdated

    switch (this.state.idSloganCurrent) {
      case 0:
        idCurrent = 0
        this.setState({ idSloganCurrent: 0 })
        sloganCurrent = this.state.sloganStudent
        break
      case 1:
        idCurrent = 1
        this.setState({ idSloganCurrent: 1 })
        sloganCurrent = this.state.sloganUniversity
        break
      case 2:
        idCurrent = 2
        this.setState({ idSloganCurrent: 2 })
        sloganCurrent = this.state.sloganBusiness
        break
    }
      
    if (this.state.indexTyped < sloganCurrent.length) {
      // document.getElementById(idCurrent).innerHTML += sloganCurrent.charAt(this.state.indexTyped)
      switch (idCurrent) {
        case 0:
          stringUpdated = this.state.sloganStudentUpdated
          stringUpdated += sloganCurrent.charAt(this.state.indexTyped)
          this.setState({ sloganStudentUpdated: stringUpdated })
          break;
        case 1:
            stringUpdated = this.state.sloganUniversityUpdated
            stringUpdated += sloganCurrent.charAt(this.state.indexTyped)
            this.setState({ sloganUniversityUpdated: stringUpdated })
          break;
        case 2:
            stringUpdated = this.state.sloganBusinessUpdated
            stringUpdated += sloganCurrent.charAt(this.state.indexTyped)
            this.setState({ sloganBusinessUpdated: stringUpdated })
          break;
      }
      i = this.state.indexTyped + 1
      this.setState({ indexTyped: i })
    }
    if (this.state.indexTyped === sloganCurrent.length) {
      this.setState({ indexTyped: 0 })

      if (this.state.idSloganCurrent == 2) {
        /*this.setState({ sloganUniversityUpdated: '' })
        this.setState({ sloganStudentUpdated: '' })
        this.setState({ sloganBusinessUpdated: '' })
        this.setState({ idSloganCurrent: 0 })
        */
        clearInterval(this.timerID);
      } else {
        let newSloganCurrent = this.state.idSloganCurrent + 1
        this.setState({ idSloganCurrent: newSloganCurrent })
      }
    }
  }

  render() {
    return (
      <Layout 
        {...this.props} 
        navmenu={false} 
        hasHeader={true} 
        hasFooter={true}
        page={"index"}
      >
        <Jumbotron>
          <Container className="mt-4 mb-2">
            <Row>
              <Col xs="12" md="6">
                <h1 className="display-4 font-size-md-down-5 mb-3">Jobby para Universidades</h1>
                <p className="text-secondary" style={{ padding: 5, fontSize: '1em', fontWeight: 700}}>
                  Gestión de procesos de prácticas profesionales.
                  <br/>
                  Haremos que las pasantías sean para
                </p>
                <p className="d-none d-xl-block" id="slogan-student" style={{ padding: 5, fontSize: '1.3em', fontWeight: 300}}>{this.state.sloganStudent}</p>
                <p className="d-none d-lg-block d-xl-none" id="slogan-student" style={{ padding: 4, fontSize: '1.1em', fontWeight: 300}}>{this.state.sloganStudent}</p>
                <p className="d-none d-md-block d-lg-none d-xl-none" id="slogan-student" style={{ margin:0, padding: 2, fontSize: '0.9em', fontWeight: 300}}>{this.state.sloganStudent}</p>
                <p className="d-xs d-md-none d-lg-none d-xl-none" id="slogan-student" style={{ margin:0, padding: 2, fontSize: '0.9em', fontWeight: 300}}>{this.state.sloganStudent}</p>

                <p className="d-none d-xl-block" id="slogan-university" style={{ padding: 5, fontSize: '1.3em', fontWeight: 300}}>{this.state.sloganUniversity}</p>
                <p className="d-none d-lg-block d-xl-none" id="slogan-university" style={{ padding: 4, fontSize: '1.1em', fontWeight: 300}}>{this.state.sloganUniversity}</p>
                <p className="d-none d-md-block d-lg-none d-xl-none" id="slogan-university" style={{ margin:0, padding: 2, fontSize: '0.9em', fontWeight: 300}}>{this.state.sloganUniversity}</p>
                <p className="d-xs d-md-none d-lg-none d-xl-none" id="slogan-university" style={{ margin:0, padding: 2, fontSize: '0.9em', fontWeight: 300}}>{this.state.sloganUniversity}</p>
                
                <p className="d-none d-xl-block" id="slogan-business" style={{ padding: 5, fontSize: '1.3em', fontWeight: 300}}>{this.state.sloganBusiness}</p>
                <p className="d-none d-lg-block d-xl-none" id="slogan-business" style={{ padding: 4, fontSize: '1.1em', fontWeight: 300}}>{this.state.sloganBusiness}</p>
                <p className="d-none d-md-block d-lg-none d-xl-none" id="slogan-business" style={{ margin:0, padding: 2, fontSize: '0.9em', fontWeight: 300}}>{this.state.sloganBusiness}</p>
                <p className="d-xs d-md-none d-lg-none d-xl-none" id="slogan-business" style={{ margin:0, padding: 2, fontSize: '0.9em', fontWeight: 300}}>{this.state.sloganBusiness}</p>
              </Col>
              <Col xs="12" md="6">
                <img className="d-none d-lg-block d-xl-none" src="/static/main-hero-lg.jpg" alt="Image Description"/>
                <img className="d-none d-md-block d-lg-none" src="/static/main-hero-md.jpg" alt="Image Description"/>
                <img className="d-none d-sm-block d-md-none" src="/static/main-hero-sm.jpg" alt="Image Description"/>
                <img className="d-none d-xl-block" src="/static/main-hero-xl.jpg" alt="Image Description"/>
                <img className="d-xs d-sm-none d-md-none" src="/static/main-hero-xs.jpg" alt="Image Description"/>
              </Col>
            </Row>
          </Container>
        </Jumbotron>
        <Row className="align-items-center">
          <Col>
            <h2 className="text-center display-4 mt-5 mb-2">Perfiles</h2>
          </Col>
        </Row>
        <Row className="pb-5">
          <Col xs="12" sm="12" md="3" className="pt-5">
            <div className="card mb-3">
              <img src="/static/coordinador.jpg" className="card-img-top" alt="..."></img>
              <div className="card-body">
                <h5 className="card-title">Coordinador</h5>
                <p className="card-text">Tiene las funcionalidad para crear perfiles tipo practicante, tutor académico y jefe empresarial.</p>
                <p className="card-text"><small className="text-muted">Total: </small><Badge href="#" color="success">12</Badge></p>
              </div>
            </div>
          </Col>
          <Col xs="12" sm="12" md="3" className="pt-5">
            <div className="card mb-3">
              <img src="/static/estudiante.jpg" className="card-img-top" alt="..."></img>
              <div className="card-body">
                <h5 className="card-title">Practicante</h5>
                <p className="card-text">Este perfil tiene las funcionalidades de cronograma de actividades, chequear sus actividades realizadas, ver notificaciones.</p>
                <p className="card-text"><small className="text-muted">Total: </small><Badge href="#" color="success">120</Badge></p>
              </div>
            </div>
          </Col>
          <Col xs="12" sm="12" md="3" className="pt-5">
            <div className="card mb-3">
              <img src="/static/tutor.jpg" className="card-img-top" alt="..."></img>
              <div className="card-body">
                <h5 className="card-title">Tutor</h5>
                <p className="card-text">Puede tener una lista de practicantes, ver su cronograma y hacer seguimiento.</p>
                <p className="card-text"><small className="text-muted">Total: </small><Badge href="#" color="success">18</Badge></p>
              </div>
            </div>
          </Col>
          <Col xs="12" sm="12" md="3" className="pt-5">
            <div className="card mb-3">
              <img src="/static/jefe.jpg" className="card-img-top" alt="..."></img>
              <div className="card-body">
                <h5 className="card-title">Jefe</h5>
                <p className="card-text">Realiza seguimiento y entrega al coordinador de prácticas.</p>
                <p className="card-text"><small className="text-muted">Total: </small><Badge href="#" color="success">19</Badge></p>
              </div>
            </div>
          </Col>
        </Row>
        <h2 className="text-center display-4 mt-2 mb-5">Comencemos...</h2>
        <p>
          <a href="#">Jobby</a> de{" "}
          <a href="#">nobit</a> integra actores de procesos de
          prácticas.
        </p>
        <p>
          Esta solución en TI integra experiencia en la gestión de las
          prácticas profesionales.
        </p>
        <p>
          Para mas información contacte con nosotros{" "}
          <a href="#">
            README.md
          </a>
        </p>
      </Layout>
    );
  }
}
