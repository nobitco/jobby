import React from "react";
import { Container, Row, Col, Input, Button, Modal, ModalHeader, ModalBody, Form, FormGroup, ModalFooter, FormFeedback, Label, Alert } from "reactstrap";
import Page from "../components/page";
import Layout from "../components/layout";
import SideNavBar from "../components/sideNavBar";
import { coordinatorMenu } from "../components/menu-models";
import AsyncData from "../components/async-data";
import { CreateUpdatePracticanteForm, CreateUpdateTutorForm, DeletePracticanteForm, DeleteTutorForm } from '../components/crud-forms'
import Router from 'next/router';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle } from 'reactstrap';
import Chart from 'chart.js';

export default class extends Page {
  static async getInitialProps({ req }) {
    // Inherit standard props from the Page (i.e. with session data)
    let props = await super.getInitialProps({ req });

    try {
      props.students = await AsyncData.getStudents(props.session.user.username);
      props.tutors = await AsyncData.getTutors(props.session.user.username);
      props.coordinator = await AsyncData.getCoordinator(props.session.user.username);
    } catch (e) {
      props.error = "Unable to fetch on server";
    }
    return props;
  }
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      context: "resumen", // It's can be practicantes || tutores || cronogramas || resumen
      selectedUsers: [],
      filterText: "",
      showModal: false,
      modalForm: "", // expected to be "create/update-form" || "delete-form"
      crudUser: null,
      students: props.students,
      tutors: props.tutors,
      chartData: [],
      schedules: props.coordinator.schedules_templates || [],
      filterTextSchedule: '',
      modalSchedule: false,
      updating: false,
      scheduleTitle: '',
      tasks: [],
      invalidScheduleTitle: false,
      msg: ''
    };

    this.handleClickAddTask = this.handleClickAddTask.bind(this);
    this.handleClickSaveSchedule = this.handleClickSaveSchedule.bind(this);
  }

  componentDidMount() {
    drawChart();
    Router.events.on('routeChangeStart', this.handleRouteChange);
  }

  componentDidUpdate() {
    if (this.state.context == 'resumen') {
      drawChart();
    }
  }

  handleRouteChange = url => {
    console.log('App is changing to: ', url)
  }

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  handleOnSearch = filterText => {
    this.setState({ filterText: filterText });
  };

  handleListItemSelection = clickedItem => {
    const { isChecked, item } = clickedItem;
    let selectedUsers = [...this.state.selectedUsers];
    var index = this.state.selectedUsers.indexOf(item);
    if (index < 0 && isChecked) {
      selectedUsers.push(item);
    } else {
      selectedUsers.splice(index, 1);
    }
    this.setState({ selectedUsers: selectedUsers });
  };

  toggleAllSelection = (isChecked, listItems) => {
    let selectedUsers = [...this.state.selectedUsers];

    isChecked
      ? listItems.forEach(item => {
          selectedUsers.indexOf(item) < 0 && selectedUsers.push(item);
        })
      : (selectedUsers.length = 0);
    this.setState({ selectedUsers: selectedUsers });
    console.log(isChecked + "    " + selectedUsers);
  };

  handleContextChange = selectedItem => {
    console.log('handleContextChange!');
    console.log('role:' + this.props.session.user.role);
    if (selectedItem.id !== this.state.context) {
      if (this.state.selectedUsers.length > 0) {
        if (
          confirm(
            "Tienes usuarios elementos seleccionados. ¿Está seguro de cambiar de contexto(los elementos seleccionados se eliminarán)?"
          )
        ) {
          this.setState({ selectedUsers: [] });
          this.setState({ context: selectedItem.id });
        }
      } else {
        this.setState({ context: selectedItem.id });
      }
    }
  };

  toggleModal = e =>
  this.setState({
    showModal: !this.state.showModal,
    modalForm: "",
    crudUser: null
  });

  // this function handle modal forms
  setModalForm = (modalForm, user) => {
    this.setState({
      showModal: !this.state.showModal,
      modalForm: modalForm,
      crudUser: user
    });
    
  };

  handleSubmitCreatingUpdatingUser = async (msg, operation) => {
    this.setState({ modalResponse: msg, showModal: false })
    try {
      if (this.state.context === 'practicantes') this.setState({ students: await AsyncData.getStudents(this.props.session.user.username)});
      if (this.state.context === 'tutores') this.setState({ tutors: await AsyncData.getTutors(this.props.session.user.username)});
    } catch(e) {
      console.log(e)
    }
  };

  // when user does click on edit icon 
  handleClickEditScheduleItem = (e, id) => {
    e.preventDefault()
    console.log(id)

    // To assign values of schedule to states of the modal
    this.setState({ scheduleTitle: this.state.schedules[id - 1].scheduleTitle, tasks: this.state.schedules[id - 1].tasks })

    // swith flag updating
    this.setState({ updating: true })
    this.setState({ idScheduleUpdating: e.target.value })
    this.setState(prevState => ({
      modalSchedule: !prevState.modalSchedule
    }));   
  }

  // when user does click on deleted icon
  handleClickDeleteScheduleItem = (e) => {
    e.preventDefault()
    var value = e.target.value
    console.log(e.target.value)
    console.log('delete item schedule')
  }

  handleChangeFilterTextSchedule = filterText => {
    this.setState({ filterTextSchedule: filterText })
  }

  handleToggleSchedule = (e) => {
    e.preventDefault()
    // set flag to false because it creates a new schedule
    this.setState({ updating: false })
    // it does empty the states of modal
    this.setState({ tasks: []})
    this.setState({ scheduleTitle: '' })

    this.setState( prevState => ({ modalSchedule: !prevState.modalSchedule }))
    let aux = this.state.tasks
    console.log(aux)
    console.log('toogle schedule modal')
  }

  handleSubmit(e) {
    e.preventDefault()
  }

  // set changes for values to input
  handleChangesSchedule = e => ( this.setState({ [e.target.name]: e.target.value }) )

  // add task to list
  handleClickAddTask(e) {
    e.preventDefault()
    console.log('handleClickAddTask')
    let tasks = this.state.tasks
    let newId
    // get a value for task's id
    (tasks.length === 0) ? newId = 1 : newId = tasks.length + 1
    // put a new task in list
    tasks.push({id:newId, nameTitle: 'taskTitle_' + newId, taskTitle: '', nameDate: 'taskDate_'+ newId, taskDate: '' })
    this.setState({ tasks: tasks})
  }

  // set changes for name's value of a task
  handleChangeTaskName = e => { 
    var i
    let tasksEdited = this.state.tasks
    this.state.tasks.forEach(function(t, index) {
      if (t.nameTitle == e.target.name) {
        // let removedItem = newschedules.splice(index, 1,)
        i = index
        // console.log(index)
      }
    })
    // console.log(e.target)
    // console.log(this.state.tasks[i])
    let taskEdited = this.state.tasks[i]
    taskEdited.taskTitle = e.target.value
    let removedItem = tasksEdited.splice(i, 1, taskEdited)
    this.setState({ tasks: tasksEdited })
    // console.log(taskEdited)
  }

  // delete a task
  handleClickDeleteTask(e, i) {
    let newtasks
    e.preventDefault()
    console.log('delete task')
    console.log(e.target)
    console.log(i)

    newtasks = this.state.tasks
    
    newtasks.forEach(function(t, index) {
      let nameCurrent
      nameCurrent = 'taskDate_' + i 
      if (t.nameDate == nameCurrent) {
        let removedItem = newtasks.splice(index, 1)
        
      }
    })

    this.setState({ tasks: newtasks })
  }

  // when user does click in save schedule
  async handleClickSaveSchedule(e) {
    e.preventDefault()
    console.log('handleClickSaveSchedule')
    let schedules = this.state.schedules
    let newId
    let newSchedule
    let invalid=false
    

    if(this.state.scheduleTitle.length == 0) {
      this.setState({ invalidScheduleTitle: true })
      invalid = true
    }   

    if (!invalid) {
      if (this.state.updating) {
        newSchedule = {id:this.state.idScheduleUpdating, scheduleTitle:this.state.scheduleTitle, tasks: this.state.tasks}
        let removedItem = schedules.splice(this.state.idScheduleUpdating - 1, 1, newSchedule)
      } else {
        // get a index
        (schedules.length === 0) ? newId = 1 : newId = schedules.length + 1
        // add new schedule to array
        newSchedule = {id:newId, scheduleTitle:this.state.scheduleTitle, tasks: this.state.tasks}
        schedules.push(newSchedule)
      }

      let schedules_templates = { 'schedules_templates' : this.state.schedules }
      let response = await AsyncData.saveSchedules(this.props.session.user.username, schedules_templates)
      this.setState({ msg: response.msg})
      // it does blank the states
      this.setState({ scheduleTitle: '' })
      this.setState({ tasks: []})

      this.setState(prevState => ({
        modalSchedule: !prevState.modalSchedule
      }));
    }
  }

 // set changes for task's date 
 handleChangeTaskDate = e => { 
  var i
  let tasksEdited = this.state.tasks
  this.state.tasks.forEach(function(t, index) {
    if (t.nameDate == e.target.name) {
      // let removedItem = newschedules.splice(index, 1)
      i = index
      // console.log(index)
    }
  })
  // console.log(e.target)
  console.log('change date')
  // console.log(this.state.tasks[i])
  let taskEdited = this.state.tasks[i]
  var newunixtime = Date.parse(e.target.value)
  taskEdited.taskDate = e.target.value

  // remove item
  let removedItem = tasksEdited.splice(i, 1)

  tasksEdited.forEach(function(t, index) {
    let currentunixtime = Date.parse(t.taskDate)
    if (newunixtime < currentunixtime) {
      i = index    
    }
  })
  
  removedItem = tasksEdited.splice(i, 0, taskEdited)
  this.setState({ tasks: tasksEdited })
  // console.log(taskEdited)
}

  //sets the content according to state.context
  getContent = context => {
    switch (context) {
      case "resumen":
        return <Statistics assessments={this.props.chartData} />;
        break;
      case "practicantes":
        return (
          <div>
            <Button
              color="primary"
              className="float-right"
              onClick={e => this.setModalForm("create/update-form", null)}
            >
              Nuevo Practicante
            </Button>
          <ListLayout
            items={this.state.students}
            selectedItems={this.state.selectedUsers}
            onItemSelected={this.handleListItemSelection}
            onToggleAllSelection={this.toggleAllSelection}
            onSearch={this.handleOnSearch}
            searchText={this.state.filterText}
            filterBy={"name"}
            onEditItem={this.setModalForm}
          />
          </div>
        );
        break;
      case "tutores":
        return (
          <div>
            <Button
              color="primary"
              className="float-right"
              onClick={e => this.setModalForm("create/update-form", null)}
            >
              Nuevo tutor
            </Button>
            <ListLayout
              items={this.state.tutors}
              selectedItems={this.state.selectedUsers}
              onItemSelected={this.handleListItemSelection}
              onToggleAllSelection={this.toggleAllSelection}
              onSearch={this.handleOnSearch}
              searchText={this.state.filterText}
              filterBy={"name"}
              onEditItem={this.setModalForm}
            />
          </div>
        );
        break;
      case "cronogramas":
        return (
          <div>
            <Button
              color="primary"
              className="float-right"
              onClick={e => this.handleToggleSchedule(e) }
            >
              Nuevo cronograma
            </Button>

            <SchedulesList schedules={ this.state.schedules } 
                            handleClickEditScheduleItem={ this.handleClickEditScheduleItem } 
                            handleClickDeleteScheduleItem={ this.handleClickDeleteScheduleItem }
                            handleChangeFilterTextSchedule={ this.handleChangeFilterTextSchedule }
                            filterTextSchedule = {this.state.filterTextSchedule }/>
          </div>
        );
        break;
      default:
        return <Estatistics assessments={this.props.assessments} />;
    }
  };

  render() {
    let headerTitle, buttonSaveUpdateTitle

    this.state.updating ? headerTitle = 'Updating Schedule' : headerTitle = 'New Schedule'
    this.state.updating ? buttonSaveUpdateTitle = 'Update schedule' : buttonSaveUpdateTitle = 'Save schedule'

    if (!this.props.session.user) return super.adminAcccessOnly();
    return (
      <Layout
        {...this.props}
        hasHeader={true}
        hasFooter={false}
        page={"dashboard"}
      >
      <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
        <ModalHeader toggle={this.toggleModal}>
          {(!this.state.crudUser)
            ? 'Crear '
            : ''}
          {(this.state.crudUser && this.state.modalForm === 'create/update-form')
            ? 'Actualizar '
            : ''}

          {(this.state.crudUser && this.state.modalForm === 'delete-form')
            ? 'Eliminar '
            : ''
          }
          {(this.state.context === 'practicantes') ? 'practicante' : ''}
          {(this.state.context === 'tutores') ? 'tutor' : ''}
        </ModalHeader>
        <ModalBody>
          {(this.state.context === "practicantes" && this.state.modalForm === 'create/update-form') && (
            <CreateUpdatePracticanteForm
              onSubmit={this.handleSubmitCreatingUpdatingUser}
              onCancel={this.toggleModal}
              user={this.state.crudUser}
              coordinator={this.props.session.user.username}
            />
          )}
          {(this.state.context === "tutores" && this.state.modalForm === 'create/update-form') && (
            <CreateUpdateTutorForm
              onSubmit={this.handleSubmitCreatingUpdatingUser}
              onCancel={this.toggleModal}
              user={this.state.crudUser}
              coordinator={this.props.session.user.username}
            />
          )}
          {(this.state.context === "practicantes" && this.state.modalForm === 'delete-form') && (
            <DeletePracticanteForm
              onSubmit={this.handleSubmitCreatingUpdatingUser}
              onCancel={this.toggleModal}
              user={this.state.crudUser}
            />
          )}
          {(this.state.context === "tutores" && this.state.modalForm === 'delete-form') && (
            <DeleteTutorForm
              onSubmit={this.handleSubmitCreatingUpdatingUser}
              onCancel={this.toggleModal}
              user={this.state.crudUser}
            />
          )}
        </ModalBody>
      </Modal>

      <Row>
        {this.props.session.user && (
          <Col sm="2" md="3" lg="2" xl="2" id="sidebar-container">
            <SideNavBar
              onContextChange={this.handleContextChange}
              navItems={coordinatorMenu}
              currentContext={this.state.context}
            />
          </Col>
        )}
        <Col
          xs={{ size: 12}}
          sm={{ size: 10}}
          md={{ size: 9}}
          lg={{ size: 10}}
          xl={{ size: 10}}
          id="content-container"
        >
          <Alert color="warning">
            { this.state.msg }
          </Alert>
          {this.props.session.user && this.props.session.user.role === '2'
            ? this.getContent(this.state.context)
            : super.notAuthorized()}
        </Col>
      </Row>

      <Modal isOpen={this.state.modalSchedule} toggle={this.handleToggleSchedule} size="xl">
          <ModalHeader toggle={this.handleToggleSchedule}>{ headerTitle }</ModalHeader>
          <ModalBody>
            <div className="container-fluid">
              <Form method="post" onSubmit={ this.handleSubmit } noValidate>
                <div className="row">
                  <div className="col">
                    <FormGroup>
                      <Label for="scheduleTitle">Schedule title</Label>
                      <Input 
                        type="text"
                        name="scheduleTitle"
                        id="schedule_title"
                        placeholder="Type schedule title..."
                        value={ this.state.scheduleTitle }
                        onChange={ this.handleChangesSchedule } 
                        invalid = { this.state.invalidScheduleTitle }/>
                      <FormFeedback>¡Título cronograma es inválido!</FormFeedback>
                    </FormGroup>
                  </div>
                </div>
                <div className="row m-3 justify-content-center">
                  <div className="col-6">
                    <Button color="success" onClick={ this.handleClickAddTask }>Add task</Button>{' '}
                  </div>
                </div>
                { this.state.modalSchedule && (
                  this.state.tasks.map((t) => 
                    <ItemTask key={t.id} task={ t } handleChangeTaskName = { this.handleChangeTaskName }  handleChangeTaskDate = { this.handleChangeTaskDate } handleClickDeleteTask = { this.handleClickDeleteTask }/>
                  )
                )}
              </Form>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.handleClickSaveSchedule}>{ buttonSaveUpdateTitle }</Button>{' '}
            <Button color="secondary" onClick={this.handleToggleSchedule}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </Layout>
    );
  }
}

function Assesments(props) {
  let { items } = props;
  return (
    <ul className="list-group" id="assesments-list">
      {items !== undefined ? (
        items.map((item, key) => (
          <li className="list-group-item-action list-item" key={key}>
            <span className="float-left">{item.order}</span>
            <div>
              <a className="d-flex" href="">
                <h2>{item.title}</h2>
              </a>
              <small>Se entrega a los {item.startAt} días</small>
              <p>{item.description}</p>
            </div>
          </li>
        ))
      ) : (
        <h3 style={{ marginTop: 40 }}>No cronogramas creadas en tu cuenta :/</h3>
      )}
    </ul>
  );
}

function Statistics(props) {
  return (
    <Container>
      <Row className="pb-5">
        <Col xs="12" sm="12" lg="6">
          <Card>
            <CardBody>
              <CardTitle>Practicantes</CardTitle>
              <CardText>Número de practicantes en plataforma.</CardText>
              <canvas id="students-chart"></canvas>
              <Button>ver mas</Button>
            </CardBody>
          </Card>
        </Col>
        <Col xs="12" sm="12" lg="6">
          <Card>
            <CardBody>
              <CardTitle>Tutores académicos</CardTitle>
              <CardText>Número de tutores académicos en plataforma.</CardText>
              <canvas id="tutors-chart"></canvas>
              <Button>ver mas</Button>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row className="pb-5">
        <Col xs="12" sm="12" lg="6">
          <Card>
            <CardBody>
              <CardTitle>Empresas</CardTitle>
              <CardText>Número de empresas en plataforma.</CardText>
              <canvas id="enterprises-chart"></canvas>
              <Button>ver mas</Button>
            </CardBody>
          </Card>
        </Col>
        <Col xs="12" sm="12" lg="6">
          <Card>
            <CardBody>
              <CardTitle>Tutores empresariales</CardTitle>
              <CardText>Número de tutores empresariales.</CardText>
              <canvas id="businesstutors-chart"></canvas>
              <Button>ver mas</Button>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}

function ListLayout(props) {
  const filteredItems =
    props.items !== undefined
      ? props.items.filter(item => filtrar(item))
      : props.items;
  //filters per fullname user property, but it should iterate every item key and check if its value coincides!
  function filtrar(obj) {
    let isName;
    let isLastname;
    let name;
    let lastname;
    let userdata;
    let item;

    isName = false;
    isLastname = false;

    userdata = obj["userdata"];
    item = userdata[0]
    name = item.name
    lastname = item.lastname

    name
      .toLowerCase()
      .indexOf(props.searchText.toLowerCase()) >= 0
      ? isName = true
      : isLastname = false;

    lastname
      .toLowerCase()
      .indexOf(props.searchText.toLowerCase()) >= 0
      ? isName = true
      : isLastname = false;
    
      return((isName || isLastname) ? true : false);
  }

  return (
    <div>
      <div className="search-bar-container">
        <i className="fas fa-search" />
        <Input
          type="text"
          onChange={e => props.onSearch(e.target.value)}
          className="search-input"
          placeholder="Buscar"
          value={props.searchText}
        />
      </div>
      <div id="list-info-container">
        <Input
          type="checkbox"
          onChange={e =>
            props.onToggleAllSelection(e.target.checked, props.items)
          }
          checked={props.selectedItems.length > 0 ? true : false}
        />
        <p>{filteredItems !== undefined && filteredItems.length} Resultados</p>
      </div>
      <ul className="list-group">
        {filteredItems !== undefined ? (
          filteredItems.map((item, key) => (
            <ListItem
              key={key}
              onSelected={props.onItemSelected}
              isSelected={props.selectedItems.indexOf(item) < 0 ? false : true}
              onEdit={props.onEditItem}
              item={item}
            />
          ))
        ) : (
          <h3 style={{ marginTop: 40 }}>No hay resultados para mostrar :/</h3>
        )}
      </ul>
    </div>
  );
}

function ListItem(props) {
  //lift the selected item up and its checkbox current on/off state to add or remove it
  //in the selectedUsers array state
  function handleChange(e) {
    const clickedItem = {
      isChecked: e.target.checked,
      item: props.item
    };
    props.onSelected(clickedItem);
  }

  const { item } = props;
  return (
    <li
      className={
        "list-group-item list-group-item-action list-item" +
        (props.isSelected ? " selected" : "")
      }
    >
      <Input
        type="checkbox"
        onChange={handleChange}
        className="item-checkbox"
        id={item.id}
        checked={props.isSelected}
      />
      { !item.avatar && <img src="/static/user.png" className="avatar" /> }
      <div className="item-info">
        <h3 className="mb-1">{item.userdata[0].name + ' ' + item.userdata[0].lastname}</h3>
        <p className="mb-1">{item.username}</p>
        <small>
          {item.city}
          <span className="badge badge-danger badge-pill">14</span>{" "}
        </small>
      </div>

      <div className="item-opts">
        <button
          href=""
          className="edit-btn"
          onClick={e => props.onEdit("create/update-form", item)}
        >
          <i className="material-icons">&#xe254;</i>
        </button>
        <button
          href=""
          className="delete-btn"
          onClick={e => props.onEdit("delete-form", item)}
        >
          <i className="material-icons">&#xe872;</i>
        </button>
      </div>
    </li>
  );
}

function ScheduleItem (props) {
  return(
    <li
    className={
      "list-group-item list-group-item-action list-item" +
      (props.isSelected ? " selected" : "")
    }
    >
      <div className="item-info">
        <h3 className="mb-1">{ props.schedule.scheduleTitle }</h3>
        <p className="mb-1">{ props.schedule.scheduleTitle }</p>
      </div>

      <div className="item-opts">
        <button
          href=""
          className="edit-btn"
          value={ props.schedule.id }
        >
          <i className="material-icons" value={ props.schedule.id } onClick = { e => props.handleClickEditScheduleItem(e, props.schedule.id ) }>&#xe254;</i>
        </button>
        <button
          href=""
          className="delete-btn"
          onClick={ props.handleClickDeleteScheduleItem }
        >
          <i className="material-icons">&#xe872;</i>
        </button>
      </div>
    </li>
  )
}

function SchedulesList (props) {
  // filter schedules by filterText
  let scheduleFiltered = props.schedules.filter(schedule => filterSchedule(schedule))

  function filterSchedule(s) {
    let selected
    s.scheduleTitle.toLowerCase().indexOf(props.filterTextSchedule.toLowerCase()) >= 0 ? selected = true : selected = false;
    
    return selected
  }
  
  return (
    <div>
      <div className="search-bar-container">
        <i className="fas fa-search" />
        <Input
          type="text"
          onChange={e => props.handleChangeFilterTextSchedule(e.target.value)}
          className="search-input"
          placeholder="Buscar"
          value={props.filterTextSchedule}
        />
      </div>
      <div id="list-info-container">
        <Input
          type="checkbox"
        />
        <p>Resultados</p>
      </div>
       <ul className="list-group">
        { scheduleFiltered.map((s) =>
          <ScheduleItem key={ s.id } schedule={ s } 
                        handleClickEditScheduleItem={ props.handleClickEditScheduleItem }
                        handleClickDeleteScheduleItem={props.handleClickDeleteScheduleItem} />
        )}
       </ul>
    </div>
  );
}

function ItemTask(props) {
  let nameTitle, idTitle, nameDate, idDate
  nameTitle = "taskTitle_" + props.task.id
  idTitle = "task_title_" + props.task.id
  nameDate = "taskDate_" + props.task.id
  idDate = "task_date_" + props.task.id

  return(
    <div>
    <div className="row align-items-center no-gutters">
      <div className="col-12 col-md-6 align-self-center">
        <FormGroup>
          <Label for="scheduleTitle">Task # { props.task.id }</Label>
          <Input
            type="text"
            name={ nameTitle }
            id={ idTitle }
            placeholder="Type title task..."
            value={ props.task.taskTitle }
            onChange={ props.handleChangeTaskName }
            bsSize="sm"
          />
        </FormGroup>
      </div>
      <div className="col-12 col-md-5 align-self-center">
        <FormGroup>
          <Label for="scheduleTitle">Due</Label>
          <Input
            type="date"
            name={ nameDate }
            id= { idDate }
            placeholder="date placeholder"
            value={ props.task.taskDate }
            onChange={ props.handleChangeTaskDate }
            bsSize="sm"
          />
        </FormGroup>
      </div>
      <div className="col-12 col-md-1 offset-11">
        <button type="button" className="btn btn-link"><i className="material-icons" value={ props.task.id }  onClick = { e => props.handleClickDeleteTask(e, props.task.id) } >restore_from_trash</i></button>
      </div>
  </div>
  <hr></hr>
  </div>
  )
}

function drawChart() {
  let dataStudents = {
    datasets: [{ 
      data: [10, 20, 30],
      backgroundColor: ["#004F94", "#2D779E", "#063266", "#EDB600"]
    }],
    labels: [
      'Sin iniciar', 'En proceso', 'Culminado'
    ]
  };
  let ctxStudentsChart = document.getElementById('students-chart');
  let studentsChart = new Chart( ctxStudentsChart, {
    type: 'pie',
    data: dataStudents,
    options: {}
  });

  let dataTutors = {
    datasets: [{ 
      data: [60, 30],
      backgroundColor: ["#2D779E", "#063266", "#EDB600"]
    }],
    labels: [
      'Activos', 'Inactivos'
    ]
  };
  let ctxTutorsChart = document.getElementById('tutors-chart');
  let tutorsChart = new Chart( ctxTutorsChart, {
    type: 'pie',
    data: dataTutors,
    options: {}
  });

  let dataEnterprises = {
    datasets: [{ 
      data: [140, 30, 344],
      backgroundColor: ["#2D779E", "#063266", "#EDB600", "#ED9600"]
    }],
    labels: [
      'Activos', 'Inactivos'
    ]
  };
  
  let ctxEnterprisesChart = document.getElementById('enterprises-chart');
  let enterprisesChart = new Chart( ctxEnterprisesChart, {
    type: 'pie',
    data: dataEnterprises,
    options: {}
  });

  let dataBusinessTutors = {
    datasets: [{ 
      data: [140, 30, 344],
      backgroundColor: ["#2D779E", "#063266", "#EDB600", "#ED9600"]
    }],
    labels: [
      'Activos', 'Inactivos', 'off', 'on'
    ]
  };
  
  let ctxEnterprisesTutorsChart = document.getElementById('businesstutors-chart');
  let enterprisesTutorsChart = new Chart( ctxEnterprisesTutorsChart, {
    type: 'pie',
    data: dataBusinessTutors,
    options: {}
  });
}
