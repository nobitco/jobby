import React from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import Router from "next/router";
import AsyncData from "./async-data";

export default class CreateUserForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: this.props.user.username,
      usernameCurrent: this.props.user.username,
      password: "",
      fullname: this.props.user.fullname,
      role: this.props.user.role
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  async handleSubmit(event) {
    event.preventDefault();
    let msg;

    const formData = {
      usernameCurrent: this.state.usernameCurrent,
      username: this.state.username,
      password: this.state.password,
      fullname: this.state.fullname,
      role: this.state.role
    };
    console.log(formData);
    if (this.props.modeForm) {
      // create user
      try {
        const response = await AsyncData.saveUser(formData);
        msg = "¡Usuario guardado!";
        this.setState({
          username: "",
          password: ""
        });
      } catch (e) {
        alert("error save user");
      }
    } else {
      // update user
      try {
        const response = await AsyncData.updateUser(formData);
        msg = "¡Usuario actualizado!";
      } catch (e) {
        console.log(e);
        alert("error update user");
      }
    }
    this.props.alertShow(msg);
    this.props.toggle();
  }

  render() {
    const isNew = this.props.modeForm;

    return (
      <Form method="post" action="/user" onSubmit={this.handleSubmit}>
        <FormGroup>
          <Label for="username">Username</Label>
          <Input
            type="email"
            name="username"
            id="email"
            placeholder="Escriba su email"
            onChange={this.handleChange}
            value={this.state.username}
          />
        </FormGroup>
        <FormGroup>
          <Label for="password">Password</Label>
          <Input
            type="password"
            name="password"
            id="examplePassword"
            placeholder="Escriba su password"
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup>
          <Label for="names">Nombre completo</Label>
          <Input
            type="text"
            name="fullname"
            id="fullname"
            placeholder="Nombre completo"
            onChange={this.handleChange}
            value={this.state.fullname}
          />
        </FormGroup>
        <FormGroup>
          <Label for="role">Perfil</Label>
          <Input
            type="select"
            name="role"
            id="roleSelect"
            onChange={this.handleChange}
          >
            <option value="1">Coordinador</option>
            <option value="2">Practicante</option>
            <option value="3">Empresa</option>
          </Input>
        </FormGroup>
        {isNew ? (
          <Button id="submitButton">Guardar</Button>
        ) : (
          <Button id="submitButton">Actualizar</Button>
        )}{" "}
        <Button id="cancelButton">Cancelar</Button>
      </Form>
    );
  }
}
