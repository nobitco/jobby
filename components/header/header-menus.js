import React from "react";
import Link from "next/link";
import Router from "next/router";
import { Form, Button, Nav, NavLink, NavItem, UncontrolledDropdown, DropdownMenu, DropdownItem, DropdownToggle } from "reactstrap";

const UserMenu = function(props) {
  let { user } = props.session;
  const adminMenuItem = user.admin && (
    <Link prefetch href="/admin">
      <a href="/admin" className="dropdown-item">
        <span className="icon ion-md-settings mr-1" /> Admin setup
      </a>
    </Link>
  );
  let dashboardType;
  if (props.session.user.admin) {
    dashboardType = 'dashboard-admin';
  } else {
    switch(props.session.user.role) {
      case '2':
        dashboardType = 'dashboard-coordinador';
        break;
      case '3':
        dashboardType = 'dashboard-tutor';
        break;
      case '4':
        dashboardType = 'dashboard-practicante';
        break;
    }
  }
  return (
    <Nav className="ml-auto" navbar>
    <UncontrolledDropdown nav inNavbar>
      <DropdownToggle nav caret className={ props.page == 'index' ? "nav-item-index" : "nav-item-dash" }>
        {user.name + ' ' + user.lastname}
        <span
          className="icon ion-md-contact ml-2 mr-1"
          style={{ fontSize: "1.4em" }}
        />
      </DropdownToggle>
      <DropdownMenu right>
        <DropdownItem>
          <NavLink href="/account"><span className="icon ion-md-person mr-1" /> Cuenta</NavLink>
        </DropdownItem>
        <DropdownItem>
          <NavLink href={ dashboardType }><span className="icon ion-md-home mr-1" /> Dashboard</NavLink>
        </DropdownItem>
        <DropdownItem divider />
        <Form
            id="signout"
            method="post"
            action="/auth/signout"
            onSubmit={() => props.onSubmit}
          >
            <input
              name="_csrf"
              type="hidden"
              value={props.session.csrfToken}
            />
            <DropdownItem
              type="submit"
              block="true"
            >
              <span className="icon ion-md-log-out mr-1" /> Salir
            </DropdownItem>
          </Form>
      </DropdownMenu>
    </UncontrolledDropdown>
  </Nav>
  );
};

const SignupMenu = function(props) {
  return (
    <Nav className="ml-auto" navbar>
      <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
          Documentación
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem>
            Desarrolladores
          </DropdownItem>
          <DropdownItem>
            Coordinadores
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem>
            Empresas
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
      <NavItem>
        <NavLink href="#">Contáctanos</NavLink>
      </NavItem>
      <NavItem>
        <NavLink className="btn btn-greenhome" href="/login">Login</NavLink>
      </NavItem>
      <NavLink> </NavLink>
      <NavItem>
        <NavLink className="btn btn-bluehome" href="/login" onClick={ props.onToggleModal }>Registrarse</NavLink>
      </NavItem>
    </Nav>
  );
};

export { UserMenu, SignupMenu };
