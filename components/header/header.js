import React from "react";
import Link from "next/link";
import { Navbar, NavbarBrand, Collapse, NavbarToggler, Nav } from "reactstrap";
import { SignupMenu, UserMenu } from "./header-menus";

export default function Header(props) {
  function getRightMenu() {
    //console.log(props.session.user ? "Estoy loggeado" : "No estoy loggeado");

    if (props.session && props.session.user) {
      //check if user is logged, decides show diferent header menus options
      return (
        <UserMenu
          session={props.session}
          onToggleModal={props.onToggleModal}
          onSubmit={props.onSubmit}
          page={props.page}
        />
      );
    } else {
      return (
        <SignupMenu onToggleModal={props.onToggleModal} />
      );
    }
  }

  function isSelectedClass(pageName) {
    return pageName === props.page ? "selected" : "";
  }

  function getColorBackground() {
    return props.page == 'index' ? "white" : "primary"
  }

  function getColor() {
    return props.page == 'index' ? "primary" : "white"
  }
  return (
    <Navbar color={getColorBackground()} className="pt-3 pb-3" expand="md" id="site-navbar">
      <NavbarBrand style={{ color: getColor() }} href="/">
        <h3>{props.siteTitle}</h3>
      </NavbarBrand>
      <NavbarToggler
        onClick={props.onToggleNav}
      />
      <Collapse isOpen={props.isNavOpen} navbar>
        { getRightMenu() }
      </Collapse>
    </Navbar>
  );
}
