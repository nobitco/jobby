import React from "react";
import { Nav, NavItem, NavLink } from "reactstrap";

const sideNavBar = function(props) {
  function makeNav(items) {
    return items.map((item, key) => {
      return (
        <NavItem key={key}>
          <NavLink
            href="#"
            onClick={e => props.onContextChange(item)}
            active={props.currentContext === item.id ? true : false}
            style={{ display: "flex", alignItems: "center" }}
            className="sidebar-navLink"
          >
            <i className={item.iconClassName} />
            <div className="d-none d-sm-none d-md-block">{item.title}</div>
          </NavLink>
        </NavItem>
      );
    });
  }
  return (
    <Nav vertical = { false } pills style={{ paddingTop: 40 }} id="side-bar">
      {makeNav(props.navItems)}
    </Nav>
  );
};
export default sideNavBar;
