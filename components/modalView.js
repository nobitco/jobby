import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";

const ModalView = function(props) {
  return (
    <Modal
      isOpen={props.modal}
      toggle={props.onToggleModal}
      style={{ maxWidth: 700 }}
    >
      <ModalHeader>{props.headerTitle}</ModalHeader>
      <ModalBody style={{ padding: "1em 2em" }}>{props.children}</ModalBody>
    </Modal>
  );
};

export default ModalView;
