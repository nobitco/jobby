const coordinatorMenu = [
  {
    title: "Resumen",
    id: "resumen",
    iconClassName: "fas fa-home"
  },
  {
    title: "Practicantes",
    id: "practicantes",
    iconClassName: "fas fa-book-reader"
  },
  {
    title: "Tutores",
    id: "tutores",
    iconClassName: "fas fa-graduation-cap"
  },
  {
    title: "Cronograma",
    id: "cronogramas",
    iconClassName: "fas fa-calendar"
  }
];

const adminMenu = [
  {
    title: "Resumen",
    id: "resumen",
    iconClassName: "fas fa-home"
  },
  {
    title: "Usuarios",
    id: "usuarios",
    iconClassName: "fas fa-book-reader"
  }
];

const tutorMenu = [
  {
    title: "Practicantes",
    id: "practicantes",
    iconClassName: "fas fa-book-reader"
  },
  {
    title: "Entregas",
    id: "entregas",
    iconClassName: "fas fa-city"
  }
];

const studentMenu = [
  {
    title: "Entregas",
    id: "entregas",
    iconClassName: "fas fa-city"
  }
];

export { studentMenu, coordinatorMenu, adminMenu, tutorMenu };
