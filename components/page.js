import React from 'react'
import Layout from '../components/layout'
import { NextAuth } from 'next-auth/client'
import Link from 'next/link'

export default class extends React.Component {
  
  static async getInitialProps({req}) {
    return {
      session: await NextAuth.init({req}),// Add this.props.session to all pages
      lang: 'en'// Add a lang property for accessibility
    }
  }
  
  adminAcccessOnly() {
    return (
      <Layout {...this.props} navmenu={false}>
        <div className="text-center pt-5 pb-5">
          <h1 className="display-4 mb-5">Acceso denegado</h1>
          <p className="lead">¡Tu debes estar autenticado para acceder a Jobby!</p>
          <Link href="/login"><a>Aquí para hacer login!</a></Link>
        </div>
      </Layout>
    )
  }

  notAuthorized() {
    return (
      <div className="text-center pt-5 pb-5">
        <h1 className="display-4 mb-5">Acceso denegado</h1>
        <p className="lead">¡No tiene privilegios!</p>
      </div>
    )
  }
}