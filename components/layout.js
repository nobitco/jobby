import React from "react";
import Router from "next/router";
import Head from "next/head";
import { Container } from "reactstrap";
import { NextAuth } from "next-auth/client";
import Cookies from "universal-cookie";
import Package from "../package";
import Styles from "../css/index.scss";
import Header from "./header/header";
import Footer from "./footer";
import ModalView from "./modalView";
import SigninForm from "./signinForm";
export default class extends React.Component {
  static propTypes() {
    return {
      session: React.PropTypes.object.isRequired,
      providers: React.PropTypes.object.isRequired,
      children: React.PropTypes.object.isRequired,
      fluid: React.PropTypes.boolean,
      navmenu: React.PropTypes.boolean,
      signinBtn: React.PropTypes.boolean
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      navOpen: false,
      modal: false,
      providers: null
    };
    this.toggleModal = this.toggleModal.bind(this);
  }

  async toggleModal(e) {
    if (e) e.preventDefault();

    // Save current URL so user is redirected back here after signing in
    if (this.state.modal !== true) {
      const cookies = new Cookies();
      cookies.set("redirect_url", window.location.pathname, { path: "/" });
    }

    this.setState({
      providers: this.state.providers || (await NextAuth.providers()),
      modal: !this.state.modal
    });
  }

  toggleNav = () => this.setState({ navOpen: !this.state.navOpen });

  async handleSignSubmit(event) {
    if (event) {
      event.preventDefault();
      // Save current URL so user is redirected back here after signing out
      const cookies = new Cookies();
      cookies.set("redirect_url", window.location.pathname, { path: "/" });

      await NextAuth.signout();
      Router.push("/");
      console.log("Cerré Sesión!");
    }
  }

  render() {
    return (
      <React.Fragment>
        <Head>
          <meta charSet="UTF-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>{this.props.title || "jobby"}</title>
          <style dangerouslySetInnerHTML={{ __html: Styles }} />
          <script src="https://cdn.polyfill.io/v2/polyfill.min.js" />
          <link
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
            rel="stylesheet"
          />
          <link
            href="https://fonts.googleapis.com/css?family=Roboto&display=swap"
            rel="stylesheet">
          </link>
          <link
            rel="stylesheet"
            href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
            integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU"
            crossOrigin="anonymous"
          />
        </Head>
        {this.props.hasHeader && (
          <Header
            siteTitle={Package.name}
            session={this.props.session}
            onToggleModal={this.toggleModal}
            onToggleNav={this.toggleNav}
            onSubmit={this.handleSignSubmit}
            isLogged={this.props.signinBtn}
            isNavOpen={this.state.navOpen}
            page={this.props.page}
          />
        )}
        <Container fluid={true}>{this.props.children}</Container>
        {this.props.hasFooter && <Footer package={Package} />}
        {this.state.providers !== null && (
          <ModalView
            modal={this.state.modal}
            onToggleModal={this.toggleModal}
            headerTitle={"Registrate!"}
          >
            <SigninForm
              session={this.props.session}
              providers={this.state.providers}
            />
          </ModalView>
        )}
      </React.Fragment>
    );
  }
}
