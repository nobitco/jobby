import React from "react";
import { Button, Form, FormGroup, Label, Input, Alert } from "reactstrap";
import AsyncData from "./async-data";

//Form used for both create user and update an existing user
//creates or updates if this.props.user has something
class CreateUpdateUserForm extends React.Component {
  constructor(props) {
    super(props);
    const { user } = this.props;
    this.state = {
      username: user ? user.username : "",
      password: "",
      name: user ? user.name : "",
      lastname: user ? user.lastname : "",    
      role: user ? user.role : "",
      error: "",
      disabled: true
    };
  }

  handleChange = e => this.setState({ [e.target.name]: e.target.value, disabled: false });

  handleSubmit = async e => {
    if (e) {
      e.preventDefault();

      this.setState({ error: "" });

      const user = {
        username: this.state.username,
        password: this.state.password,
        name: this.state.name,
        lastname: this.state.lastname,
        role: this.state.role
      };

      let invalidData = false

      if(this.state.username.length == 0) {
        let element = document.getElementById("email")
        element.classList.add("is-invalid")
        invalidData = true
      }

      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.username) !== true) {
        let element = document.getElementById("email")
        element.classList.add("is-invalid")
        invalidData = true
      }

      // Is it save new user?
      if (!this.props.user) {
        if(this.state.password.length == 0) {
          let element = document.getElementById("password")
          element.classList.add("is-invalid")
          invalidData = true
        }
        if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/.test(this.state.password) !== true) {
          let element = document.getElementById("password")
          element.classList.add("is-invalid")
          invalidData = true
        }
      }

      if(this.state.name.length == 0) {
        let element = document.getElementById("name")
        element.classList.add("is-invalid")
        invalidData = true
      }
      
      if(this.state.lastname.length == 0) {
        let element = document.getElementById("lastname")
        element.classList.add("is-invalid")
        invalidData = true
      }

      if (invalidData === false)
        try {
          //if props.user is null, creates a new user... else, updates it's info
          const response = !this.props.user
            ? await AsyncData.saveUser(user)
            : await AsyncData.updateUser(user);
          const operation = !this.props.user ? "creado" : "actualizado";
          response.success &&
            this.props.onSubmit("¡Usuario " + operation + " satisfactoriamente!", operation);
          !response.success &&
            this.setState({ error: response.msg });
        } catch (e) {
          this.setState({
            error: 'ERROR: '+ e
          });
        }
    }
  };

  render() {
    return (
      <div>
        <div>
          {this.state.error.length > 0 && (
              <Alert color="warning">{this.state.error}</Alert>
            )}
        </div>
        <Form
          autoComplete="off"
          method="post"
          action="/user"
          onSubmit={this.handleSubmit}
          className="needs-validation"
          noValidate
        >
          <FormGroup>
            <Label for="username">Username</Label>
            {this.props.user === null
            ?
              <div>
              <Input
                autoComplete="off"
                type="email"
                name="username"
                id="email"
                placeholder="Escriba su email"
                onChange={this.handleChange}
                value={this.state.username}
                required
              />
              <div className="invalid-feedback">
                Por favor, ingrese un email válido.
              </div>
              </div>
            :
              <Input
                autoComplete="off"
                type="email"
                name="username"
                id="email"
                placeholder="Escriba su email"
                onChange={this.handleChange}
                value={this.state.username}
                readOnly
              />
            }

          </FormGroup>
          <FormGroup>
            <Label for="password">Contraseña:</Label>
            <Input
              autoComplete="off"
              type="password"
              name="password"
              id="password"
              placeholder="Escriba su password"
              onChange={this.handleChange}
            />
            <small id="passwordHelpBlock" className="form-text text-muted">
              Tu contraseña debe ser de 8 hasta 15 carácteres, debe contener una minúscula y una mayúscula, debe tener al menos un número y no debe contener espacios.
            </small>
            <div className="invalid-feedback">
              Por favor ingrese una contraseña válida.
            </div>
          </FormGroup>
          <FormGroup>
              <Label for="names">Nombre(s):</Label>
              <Input
                autoComplete="off"
                type="text"
                name="name"
                className="form-control"
                id="name"
                placeholder="Ingrese aqui su nombre..."
                onChange={this.handleChange}
                value={this.state.name}
                required
              />
              <div className="invalid-feedback">
                Por favor ingrese su nombre.
              </div>
            </FormGroup>
          <FormGroup>
            <Label for="names">Apellido(s):</Label>
            <Input
              autoComplete="off"
              type="text"
              name="lastname"
              className="form-control"
              id="lastname"
              placeholder="Ingrese aqui su apellido..."
              onChange={this.handleChange}
              value={this.state.lastname}
              required
            />
            <div className="invalid-feedback">
              Por favor ingrese su apellido.
            </div>
          </FormGroup>
          <FormGroup>
            <Label for="role">Perfil</Label>
            <Input
              type="select"
              name="role"
              id="roleSelect"
              onChange={this.handleChange}
              value={this.state.role}
            >
              <option value="1">SuperAdmin</option>
              <option value="2">Coordinador</option>
              <option value="3">Tutor</option>
              <option value="4">Practicante</option>
            </Input>
          </FormGroup>
          <Button color="success" id="submitButton" className="float-right" type="submit" disabled={ this.state.disabled }>
            {!this.props.user ? "Crear usuario" : "Actualizar usuario"}
          </Button>
          <Button
            id="cancelButton"
            className="float-left"
            onClick={this.props.onCancel}
            color="primary"
          >
            Cancelar
          </Button>
        </Form>
      </div>
    );
  }
}

class DeleteForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmationText: "",
      localMsg: null
    };
  }
  handleSubmit = async e => {
    if (e) {
      const { user } = this.props;
      e.preventDefault();
      try {
        if (this.state.confirmationText === "DELETE") {
          let response = await AsyncData.deleteUser(user.username);
          response.success &&
            this.props.onSubmit(
              "¡El Usuario " + user.username + " fue eliminado exitosamente!"
            );
          !response.success &&
            this.props.onSubmit(
              "¡El Usuario " + user.username + " no puede ser eliminado!"
            );
          console.log(response.msg);
        } else {
          this.setState({
            localMsg: "Confirma la operación escribiendo en el campo DELETE"
          });
        }
      } catch (e) {
        this.props.onSubmit(
          "No se pudo eliminar al usuario " +
            user.username +
            ". Por favor, inténtalo de nuevo  "
        );
      }
    }
  };

  render() {
    return (
      <Form
        autoComplete="off"
        method="post"
        action="/user"
        onSubmit={this.handleSubmit}
      >
        <p>
          Se borrará el usuario <strong>{ this.props.user.username }</strong>.{" "}
          <br />
          Por favor digite <strong>DELETE</strong> para borrar el usuario
          permanentemente
        </p>
        {this.state.localMsg && (
          <Alert color="warning">{this.state.localMsg} </Alert>
        )}
        <Input
          className="confirmation-input"
          type="text"
          name="confirmation"
          onChange={e =>
            this.setState({ confirmationText: e.target.value.toUpperCase() })
          }
          value={this.setState.confirmationText}
        />
        <Button
          id="cancelButton"
          className="float-left"
          onClick={this.props.onCancel}
          color="primary"
        >
          Cancelar
        </Button>
        <Button color="danger" id="submitButton" className="float-right">
          Eliminar Usuario
        </Button>
      </Form>
    );
  }
}

class CreateUpdatePracticanteForm extends React.Component {
  constructor(props) {
    super(props);
    const { user } = this.props;
    this.state = {
      username: user ? user.username : "",
      password: "",
      name: user ? user.userdata[0].name : "",
      lastname: user ? user.userdata[0].lastname : "",
      role: "4",
      error: "",
      state: user ? user.state : "0",
      disabled: true
    };
  }

  handleChange = e => this.setState({ [e.target.name]: e.target.value, disabled: false});

  handleSubmit = async e => {
    if (e) {
      let student;
      e.preventDefault();
      this.setState({ error: "" });
      if (this.props.user) {
        student = {
          username: this.state.username,
          password: this.state.password,
          name: this.state.name,
          lastname: this.state.lastname,
          state: this.state.state,
          coordinator: this.props.coordinator
        };
      } else {
        student = {
          username: this.state.username,
          password: this.state.password,
          name: this.state.name,
          lastname: this.state.lastname,
          role: this.state.role,
          state: '0',
          coordinator: this.props.coordinator,
          assessments: "none"
        };
      }

      let invalidData = false

      if(this.state.username.length == 0) {
        let element = document.getElementById("email")
        element.classList.add("is-invalid")
        invalidData = true
      }

      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.username) !== true) {
        let element = document.getElementById("email")
        element.classList.add("is-invalid")
        invalidData = true
      }

      // Is it save new user?
      if (!this.props.user) {
        if(this.state.password.length == 0) {
          let element = document.getElementById("password")
          element.classList.add("is-invalid")
          invalidData = true
        }
        if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/.test(this.state.password) !== true) {
          let element = document.getElementById("password")
          element.classList.add("is-invalid")
          invalidData = true
        }
      } else {
        // it is updating a user!
        if(this.state.password.length > 0) {
          if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/.test(this.state.password) !== true) {
            let element = document.getElementById("password")
            element.classList.add("is-invalid")
            invalidData = true
          }
        }
      }
     
      if(this.state.name.length == 0) {
        let element = document.getElementById("name")
        element.classList.add("is-invalid")
        invalidData = true
      }
      
      if(this.state.lastname.length == 0) {
        let element = document.getElementById("lastname")
        element.classList.add("is-invalid")
        invalidData = true
      }

      if (invalidData === false) {
        try {
          //if props.user is null, creates a new user... else, updates it's info
          const response = !this.props.user
            ? await AsyncData.saveStudent(student)
            : await AsyncData.updateStudent(student);

          const operation = !this.props.user ? "creado" : "actualizado";
          response.success &&
            this.props.onSubmit("¡Usuario " + operation + " satisfactoriamente!", operation);
          !response.success &&
            this.setState({ error: response.msg });
          } catch (e) {
            this.setState({
              error: 'ERROR: '+ e
            });
          }
      }
    }
  }

  render() {
    return (
      <div>
        <div>
          {this.state.error.length > 0 && (
              <Alert color="warning">{this.state.error}</Alert>
            )}
        </div>
        <Form
          autoComplete="off"
          method="post"
          action="/user"
          onSubmit={this.handleSubmit}
          className="needs-validation"
          noValidate
        >
          <FormGroup>
            <Label for="username">Username</Label>
            {this.props.user === null
            ?
              <div>
              <Input
                autoComplete="off"
                type="email"
                name="username"
                id="email"
                placeholder="Ingrese aqui su email..."
                onChange={this.handleChange}
                value={this.state.username}
                required
              />
              <div className="invalid-feedback">
                Por favor, ingrese un email válido.
              </div>
              </div>
            :
              <Input
                autoComplete="off"
                type="email"
                name="username"
                id="email"
                placeholder="Ingrese aqui su email..."
                onChange={this.handleChange}
                value={this.state.username}
                readOnly
              />
            }
          </FormGroup>
          <FormGroup>
            <Label for="password">Contraseña:</Label>
            <Input
              autoComplete="off"
              type="password"
              name="password"
              id="password"
              placeholder="Ingrese aqui su contraseña..."
              onChange={this.handleChange}
              required
            />
            <small id="passwordHelpBlock" className="form-text text-muted">
              Tu contraseña debe ser de 8 hasta 15 carácteres, debe contener una minúscula y una mayúscula, debe tener al menos un número y no debe contener espacios.
            </small>
            <div className="invalid-feedback">
              Por favor ingrese una contraseña válida.
            </div>
          </FormGroup>
          <FormGroup>
            <Label for="names">Nombre(s):</Label>
            <Input
              autoComplete="off"
              type="text"
              name="name"
              className="form-control"
              id="name"
              placeholder="Ingrese aqui su nombre..."
              onChange={this.handleChange}
              value={this.state.name}
              required
            />
            <div className="invalid-feedback">
              Por favor ingrese su nombre.
            </div>
          </FormGroup>
          <FormGroup>
            <Label for="names">Apellido(s):</Label>
            <Input
              autoComplete="off"
              type="text"
              name="lastname"
              className="form-control"
              id="lastname"
              placeholder="Ingrese aqui su apellido..."
              onChange={this.handleChange}
              value={this.state.lastname}
              required
            />
            <div className="invalid-feedback">
              Por favor ingrese su nombre.
            </div>
          </FormGroup>
          <Button color="success" id="submitButton" className="float-right" type="submit" disabled={ this.state.disabled }>
            {!this.props.user ? "Crear practicante" : "Actualizar practicante"}
          </Button>
          <Button
            id="cancelButton"
            className="float-left"
            onClick={this.props.onCancel}
            color="primary"
          >
            Cancelar
          </Button>
        </Form>

      </div>
    );
  }
}

class CreateUpdateTutorForm extends React.Component {
  constructor(props) {
    super(props);
    const { user } = this.props;
    this.state = {
      username: user ? user.username : "",
      password: "",
      name: user ? user.userdata[0].name : "",
      lastname: user ? user.userdata[0].lastname : "",
      role: "3",
      error: "",
      disabled: true
    };
  }

  handleChange = e => this.setState({ [e.target.name]: e.target.value, disabled: false });

  handleSubmit = async e => {
    if (e) {

      e.preventDefault();
      this.setState({ error: "" });
      const tutor = {
        username: this.state.username,
        password: this.state.password,
        name: this.state.name,
        lastname: this.state.lastname,
        role: this.state.role,
        coordinator: this.props.coordinator
      };

      let invalidData = false

      if(this.state.username.length == 0) {
        let element = document.getElementById("email")
        element.classList.add("is-invalid")
        invalidData = true
      }

      if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.state.username) !== true) {
        let element = document.getElementById("email")
        element.classList.add("is-invalid")
        invalidData = true
      }

      // Is it save new tutor?
      if (!this.props.user) {
        if(this.state.password.length == 0) {
          let element = document.getElementById("password")
          element.classList.add("is-invalid")
          invalidData = true
        }
      } else {
        if(this.state.password.length > 0) {
          if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/.test(this.state.password) !== true) {
            let element = document.getElementById("password")
            element.classList.add("is-invalid")
            invalidData = true
          }
        }
      }

      if(this.state.name.length == 0) {
        let element = document.getElementById("name")
        element.classList.add("is-invalid")
        invalidData = true
      }
      
      if(this.state.lastname.length == 0) {
        let element = document.getElementById("lastname")
        element.classList.add("is-invalid")
        invalidData = true
      }
      
      if (invalidData === false)
        try {
          //if props.user is null, creates a new user... else, updates it's info
          const response = !this.props.user
            ? await AsyncData.saveTutor(tutor)
            : await AsyncData.updateTutor(tutor);
          const operation = !this.props.user ? "creado" : "actualizado";
          response.success &&
            this.props.onSubmit("¡Usuario " + operation + " satisfactoriamente!", operation);
          !response.success &&
          this.setState({ error: response.msg });
        } catch (e) {
          this.setState({
            error: 'ERROR: '+ e
          });
        }
    }
  };

  render() {
    return (
      <div>
        <div>
          {this.state.error.length > 0 && (
              <Alert color="warning">{this.state.error}</Alert>
            )}
        </div>
        <Form
          autoComplete="off"
          method="post"
          action="/user"
          onSubmit={this.handleSubmit}
          className="needs-validation"
          noValidate
        >
          <FormGroup>
            <Label for="username">Username</Label>
            {this.props.user === null
            ?
              <div>
              <Input
                autoComplete="off"
                type="email"
                name="username"
                id="email"
                placeholder="Escriba su email"
                onChange={this.handleChange}
                value={this.state.username}
                required
              />
              <div className="invalid-feedback">
                Por favor, ingrese un email válido.
              </div>
              </div>
            :
              <Input
                autoComplete="off"
                type="email"
                name="username"
                id="email"
                placeholder="Escriba su email"
                onChange={this.handleChange}
                value={this.state.username}
                readOnly
              />
            }

          </FormGroup>
          <FormGroup>
            <Label for="password">Contraseña:</Label>
            <Input
              autoComplete="off"
              type="password"
              name="password"
              id="password"
              placeholder="Escriba su password"
              onChange={this.handleChange}
              required
            />
            <small id="passwordHelpBlock" className="form-text text-muted">
              Tu contraseña debe ser de 8 hasta 15 carácteres, debe contener una minúscula y una mayúscula, debe tener al menos un número y no debe contener espacios.
            </small>
            <div className="invalid-feedback">
              Por favor ingrese una contraseña válida.
            </div>
          </FormGroup>
          <FormGroup>
            <Label for="names">Nombre(s):</Label>
            <Input
              autoComplete="off"
              type="text"
              name="name"
              className="form-control"
              id="name"
              placeholder="Ingrese aqui su nombre..."
              onChange={this.handleChange}
              value={this.state.name}
              required
            />
            <div className="invalid-feedback">
              Por favor ingrese su nombre.
            </div>
          </FormGroup>
          <FormGroup>
            <Label for="names">Apellido(s):</Label>
            <Input
              autoComplete="off"
              type="text"
              name="lastname"
              className="form-control"
              id="lastname"
              placeholder="Ingrese aqui su apellido..."
              onChange={this.handleChange}
              value={this.state.lastname}
              required
            />
            <div className="invalid-feedback">
              Por favor ingrese su nombre.
            </div>
          </FormGroup>
          <Button color="success" id="submitButton" className="float-right" type="submit" disabled={ this.state.disabled }>
            {!this.props.user ? "Crear tutor" : "Actualizar tutor"}
          </Button>
          <Button
            id="cancelButton"
            className="float-left"
            onClick={this.props.onCancel}
            color="primary"
          >
            Cancelar
          </Button>
        </Form>
      </div>
    );
  }
}

class DeletePracticanteForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmationText: "",
      localMsg: null
    };
  }
  handleSubmit = async e => {
    if (e) {
      const { user } = this.props;
      e.preventDefault();
      try {
        if (this.state.confirmationText === "DELETE") {
          let response = await AsyncData.deleteStudent(user.username);
          response.success &&
            this.props.onSubmit(
              "¡El Practicante " + user.username + " fue eliminado exitosamente!"
            );
          !response.success &&
            this.props.onSubmit(
              "¡El Practicante " + user.username + " no puede ser eliminado!"
            );
        } else {
          this.setState({
            localMsg: "Confirma la operación escribiendo en el campo DELETE"
          });
        }
      } catch (e) {
        this.setState({
          localMsg: 'ERROR: '+ e
        });
      }
    }
  };

  render() {
    return (
      <Form
        autoComplete="off"
        method="post"
        action="/user"
        onSubmit={this.handleSubmit}
      >
        <p>
          Se borrará el practicante <strong>{this.props.user.userdata[0].name + ' ' + this.props.user.userdata[0].lastname}</strong>.{" "}
          <br />
          Por favor digite <strong>DELETE</strong> para borrar el practicante
          permanentemente.
        </p>
        {this.state.localMsg && (
          <Alert color="warning">{this.state.localMsg} </Alert>
        )}
        <FormGroup>
          <Input
            className="confirmation-input"
            type="text"
            name="confirmation"
            onChange={e =>
              this.setState({ confirmationText: e.target.value.toUpperCase() })
            }
            value={this.setState.confirmationText}
          />
        </FormGroup>
        <Button
          id="cancelButton"
          className="float-left"
          onClick={this.props.onCancel}
          color="primary"
        >
          Cancelar
        </Button>
        <Button color="danger" id="submitButton" className="float-right">
          Eliminar Practicante
        </Button>
      </Form>
    );
  }
}

class DeleteTutorForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmationText: "",
      localMsg: null
    };
  }
  handleSubmit = async e => {
    if (e) {
      const { user } = this.props;
      e.preventDefault();
      try {
        if (this.state.confirmationText === "DELETE") {
          let response = await AsyncData.deleteTutor(user.username);
          response.success &&
            this.props.onSubmit(
              "¡El tutor " + user.username + " fue eliminado exitosamente!"
            );
          !response.success &&
            this.props.onSubmit(
              "¡El tutor " + user.username + " no puede ser eliminado!"
            );
        } else {
          this.setState({
            localMsg: "Confirma la operación escribiendo en el campo DELETE"
          });
        }
      } catch (e) {
        this.setState({
          localMsg: 'ERROR: '+ e
        });
      }
    }
  };

  render() {
    return (
      <Form
        autoComplete="off"
        method="post"
        action="/user"
        onSubmit={this.handleSubmit}
      >
        <p>
          Se borrará el tutor <strong>{this.props.user.userdata[0].name + ' ' + this.props.user.userdata[0].lastname}</strong>.{" "}
          <br />
          Por favor digite <strong>DELETE</strong> para borrar el tutor
          permanentemente
        </p>
        {this.state.localMsg && (
          <Alert color="warning">{this.state.localMsg} </Alert>
        )}
        <FormGroup>
          <Input
            className="confirmation-input"
            type="text"
            name="confirmation"
            onChange={e =>
              this.setState({ confirmationText: e.target.value.toUpperCase() })
            }
            value={this.setState.confirmationText}
          />
        </FormGroup>
        <Button
          id="cancelButton"
          className="float-left"
          onClick={this.props.onCancel}
          color="primary"
        >
          Cancelar
        </Button>
        <Button color="danger" id="submitButton" className="float-right">
          Eliminar Tutor
        </Button>
      </Form>
    );
  }
}

export { 
  CreateUpdateUserForm, 
  DeleteForm, 
  CreateUpdatePracticanteForm, 
  CreateUpdateTutorForm,
  DeletePracticanteForm,
  DeleteTutorForm
 };
