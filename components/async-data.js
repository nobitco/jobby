/**
 * This is a very simple class that returns data asynchronously.
 *
 * This code runs on both the server and in the browser.
 *
 * You could also put the logic to detect if code is being run on
 * the server or in the browser inside the page template.
 *
 * We use 'isomorphic-fetch' as it runs both server and client side.
 */
import fetch from "isomorphic-fetch";
import getConfig from "next/config";

export default class {
    static async getUsers() {
        const { publicRuntimeConfig } = getConfig();

        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "GET",
            headers: {
                Authorization: "Bearer " + token
            }
        };
        const uri = publicRuntimeConfig.API_JOBBY_URI;
        try {
            const response = await fetch(uri + "/users", options);
            if (response.status == 200) {
                const users = await response.json();
                const r = {
                    success: true,
                    data: users
                }
                return r;
            } else {
                const r = {
                    success: fail,
                    data: []
                }
                return r;
            }

        } catch (e) {
            const r = {
                success: false,
                msg: 'Failed connection to webservice ' + 'getUsers.'
            }
            return r
        }

    }

    static async saveUser(user) {
        const { publicRuntimeConfig } = getConfig();
        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "POST",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(user)
        };
        const uri = publicRuntimeConfig.API_JOBBY_URI;

        const res = await fetch(uri + "/users", options);
        if (res.status === 200) {
            const data = {
                success: true,
                msg: "Ok"
            }
            return (data)
        } else {
            const body = await res.json()
            let msg = getMsgError(body.error)
            const data = {
                success: false,
                msg: msg
            }
            return (data)
        }
    }

    static async updateUser(user) {
        const { publicRuntimeConfig } = getConfig();

        const token = publicRuntimeConfig.API_TOKEN_MONGO;
        let options = {
            method: "PUT",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(user)
        };

        const uri = publicRuntimeConfig.API_JOBBY_URI;
        const res = await fetch(uri + "/users/" + user.username, options)
        if (res.status === 200) {
            const data = {
                success: true,
                msg: "Ok"
            }
            return (data)
        } else {
            const body = await res.json()
            console.log(body.error)
            let msg = getMsgError(body.error)

            const data = {
                success: false,
                msg: msg
            }
            return (data)
        }
    }

    static async deleteUser(username) {
        const { publicRuntimeConfig } = getConfig();
        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            }
        };
        const uri = publicRuntimeConfig.API_JOBBY_URI;
        const res = await fetch(uri + "/users/" + username, options);

        if (res.status === 200) {
            const data = {
                success: true,
                msg: "deleteUser() successful operation"
            };
            return data;
        } else if (res.status === 400) {
            const body = await res.json();
            const data = {
                success: false,
                msg: "deleteUser() error operation: " + body.error
            };
            return data;
        }
    }

    static async getAssessments(username) {
        const { publicRuntimeConfig } = getConfig();

        let options = {
            method: "GET"
        };
        const uri = publicRuntimeConfig.API_FAKE_URI;
        username = 'coordinador100@icesi.edu.co';
        const res = await fetch(uri + "/assessments/" + username, options);
        const data = await res.json();
        return data;
    }

    static async getStudents(username) {
        const { publicRuntimeConfig } = getConfig();

        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "GET",
            headers: {
                Authorization: "Bearer " + token
            }
        };

        const uri = publicRuntimeConfig.API_JOBBY_URI;

        const res = await fetch(uri + "/coordinators/" + username + "/students", options);
        const data = await res.json();
        return data;
    }

    static async getTutors(username) {
        const { publicRuntimeConfig } = getConfig();

        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "GET",
            headers: {
                Authorization: "Bearer " + token
            }
        };

        const uri = publicRuntimeConfig.API_JOBBY_URI;

        const res = await fetch(uri + "/tutors/" + username, options);
        const data = await res.json();
        return data;
    }

    static async saveTutor(tutor) {
        const { publicRuntimeConfig } = getConfig();
        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "POST",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(tutor)
        };
        const uri = publicRuntimeConfig.API_JOBBY_URI;
        try {
            const res = await fetch(uri + "/tutors", options);
            if (res.status === 200) {
                const data = {
                    success: true,
                    msg: "Ok"
                }
                return (data)
            } else {
                const body = await res.json();
                let msg = getMsgError(body.error)
                const data = {
                    success: false,
                    msg: msg
                }
                return (data);
            }
        } catch (e) {
            const data = {
                success: false,
                msg: e
            }
            return (e);
        }
    }

    static async saveStudent(user) {
        const { publicRuntimeConfig } = getConfig();
        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "POST",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(user)
        };
        const uri = publicRuntimeConfig.API_JOBBY_URI;
        try {
            const res = await fetch(uri + "/students", options);
            if (res.status === 200) {
                const data = {
                    success: true,
                    msg: "Ok"
                }
                return (data)
            } else {
                const body = await res.json();
                let msg = getMsgError(body.error)
                const data = {
                    success: false,
                    msg: msg
                }
                return (data);
            }
        } catch (e) {
            const data = {
                success: false,
                msg: e
            }
            return (e);
        }

    }

    static async saveSchedules(username, schedules) {
        const { publicRuntimeConfig } = getConfig();
        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "PUT",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(schedules)
        };
        const uri = publicRuntimeConfig.API_JOBBY_URI;
        try {
            const res = await fetch(uri + "/coordinators/" + username + "/schedules", options);
            if (res.status === 200) {
                const data = {
                    success: true,
                    msg: "Ok"
                }
                return (data)
            } else {
                const body = await res.json();
                let msg = getMsgError(body.error)
                const data = {
                    success: false,
                    msg: msg
                }
                return (data);
            }
        } catch (e) {
            const data = {
                success: false,
                msg: e
            }
            return (data);
        }

    }

    static async updateStudent(student) {
        const { publicRuntimeConfig } = getConfig();

        const token = publicRuntimeConfig.API_TOKEN_MONGO;
        let options = {
            method: "PUT",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(student)
        };

        const uri = publicRuntimeConfig.API_JOBBY_URI;
        const res = await fetch(uri + "/student/" + student.username, options)
        if (res.status === 200) {
            const data = {
                success: true,
                msg: "Ok"
            }
            return (data)
        } else {
            const body = await res.json()
            console.log(body.error)
            let msg = getMsgError(body.error)

            const data = {
                success: false,
                msg: msg
            }
            return (data)
        }
    }

    static async deleteStudent(username) {
        const { publicRuntimeConfig } = getConfig();
        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            }
        };
        const uri = publicRuntimeConfig.API_JOBBY_URI;
        const res = await fetch(uri + "/student/" + username, options);

        if (res.status === 200) {
            const data = {
                success: true,
                msg: "deleteUser() successful operation"
            };
            return data;
        } else if (res.status === 400) {
            const body = await res.json();
            const data = {
                success: false,
                msg: "deleteUser() error operation: " + body.error
            };
            return data;
        }
    }

    static async updateTutor(tutor) {
        const { publicRuntimeConfig } = getConfig();

        const token = publicRuntimeConfig.API_TOKEN_MONGO;
        let options = {
            method: "PUT",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(tutor)
        };

        const uri = publicRuntimeConfig.API_JOBBY_URI;
        const res = await fetch(uri + "/tutor/" + tutor.username, options)
        if (res.status === 200) {
            const data = {
                success: true,
                msg: "Ok"
            }
            return (data)
        } else {
            const body = await res.json()
            let msg = getMsgError(body.error)

            const data = {
                success: false,
                msg: msg
            }
            return (data)
        }
    }

    static async deleteTutor(username) {
        const { publicRuntimeConfig } = getConfig();
        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "DELETE",
            headers: {
                Authorization: "Bearer " + token,
                "Content-Type": "application/json"
            }
        };
        const uri = publicRuntimeConfig.API_JOBBY_URI;
        const res = await fetch(uri + "/tutor/" + username, options);

        if (res.status === 200) {
            const data = {
                success: true,
                msg: "deleteTutor() successful operation"
            };
            return data;
        } else if (res.status === 400) {
            const body = await res.json();
            const data = {
                success: false,
                msg: "deleteTutor() error operation: " + body.error
            };
            return data;
        }
    }

    static async getSchedules(username) {
        const { publicRuntimeConfig } = getConfig();

        let options = {
            method: "GET",
            headers: {
                'Access-Control-Allow-Origin': 'http://localhost:5000'
            },
            mode: 'no-cors'
        };

        const uri = publicRuntimeConfig.API_FAKE_URI;
        username = 'coordinador100@icesi.edu.co';
        const res = await fetch(uri + "/api/schedules/" + username, options);
        console.log(uri + "/api/schedules/" + username);
        const data = await res.json();
        return data;
    }

    static async getCoordinator(username) {
        const { publicRuntimeConfig } = getConfig();

        const token = publicRuntimeConfig.API_TOKEN_MONGO;

        let options = {
            method: "GET",
            headers: {
                Authorization: "Bearer " + token
            }
        };

        const uri = publicRuntimeConfig.API_JOBBY_URI;

        const res = await fetch(uri + "/coordinators/" + username, options);
        const data = await res.json();
        return data;
    }
} // end class

function getMsgError(coderr) {
    let msg
    switch (coderr) {
        case "ERR_NOT_AUTHORIZED":
            msg = "No esta autorizado"
            break
        case "ERR_USERNAME_REQUIRED":
            msg = "Username es requerido"
            break
        case "ERR_PASSWORD_REQUIRED":
            msg = "Password es requerido"
            break
        case "ERR_USERNAME_EXISTS":
            msg = "Username ya existe"
            break
        case "ERR_NAME_REQUIRED":
            msg = "Nombre requerido"
            break
        case "ERR_LASTNAME_REQUIRED":
            msg = "Apellido requerido"
            break
        case "ERR_PASSWORD_REQUIRED":
            msg = "Contraseña requerida"
            break
        case "ERR_STATE_REQUIRED":
            msg = "Estado requerido"
            break
        case "ERR_USERNAME_COORDINATOR_REQUIRED":
            msg = "Coordinador requerido"
            break
        case "ERR_ASSESSMENTS_REQUIRED ":
            msg = "Asignaciones requerida"
            break
        case "ERR_SCHEDULES_TEMPLATE_REQUIRED ":
            msg = "Asignaciones requerida"
            break
        default:
            msg = "error server"
    }
    return msg
}