import React from "react";
import { Container } from "reactstrap";
import Link from "next/link";

const Footer = function(props) {
  return (
    <footer>
      <Container fluid={true}>
        <hr className="mt-3" />
        <p className="text-muted small">
          <Link href="#">
            <a className="text-muted font-weight-bold">
              <span className="icon ion-logo-github" /> {props.package.name}{" "}
              {props.package.version}
            </a>
          </Link>
          <span> construido por </span>
          <Link href="https://github.com/facebook/react">
            <a className="text-muted font-weight-bold">
              nobit 
            </a>
          </Link>
          .<span className="ml-2">&copy; {new Date().getYear() + 1900}.</span>
        </p>
      </Container>
    </footer>
  );
};
export default Footer;
